//
//  AppDelegate.swift
//  testScanner
//
//  Created by Kirill Gorbushko on 04.07.18.
//  Copyright © 2018 - present. All rights reserved.
//

import UIKit
import FirebaseCore
import ScanditBarcodeScanner

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let appKey = "Ae87cFhWROm5Lvn7V0ALWzVFZTuJBRkzKXkXmzFgZ3B1U/CEW2NFW5910y6qaRStPCQ6WS13LbAtcKmb5BQf9/EsZR03RDASWSbXqpxD16d/Wh1YqUD1PfcOJ2P4GdZJw53iGdzhUWOsHqS0NqFciNFH2RxMWO1phjH6mfumrQNhog3x8bFgO0a/0TSYcVWp8RhGqZc19+G63fU8kw12JvnMKVtAZ1ATsafSbSucNSsUlkzTZDabSPV+etMSTJNXezVN8Mxe8QeQASySnFaaU4qNpSZq0EUgj1R+Z6YX9Cs7aYJc2tb7+qwfk/N4TXz2ySvn/NGJqOWkxHDbKvoSr3IFIHHe40vpaSvt/E5gFVzT4EQDJtUmbUd4Syshq6RkbQMhCVmo1k70Vl/oXVB0TzCuNzyoPf9UkBssQzgn9E5MKCk9A9ns8w5x6bAtamrwA3UfahwNUZPtATF5cnUP5gzDY8QRnZludvXqpgRviq9wmD5ifQs+IGHwC3UMOYg+GGpcvwwl7GbEGrdWL47fuIpZ5ry4AWOKpwFtv+2V6kR+6Lxx33a7qv1Juv2ZIG9cU+FQaPnnIXlWpaG9U+3u9bUKBtis0Obif/CzDHuWNsHPu6lmEBLfypwasa1hKgQpn0aA6St98/s6Jo5LQs2KEtFkfUTURoqqSIDUcyKzTEPBr68/fH3siTaemxCHoyRbXLu10wApP38EsQIevwX+AIFq4CQhcSipyDLtbx0eVBSggPxkwvVU+U17d5mStOHz2Ee45erKwKyBGFZG+Fu/ntukVc2WZM2pzK7fBQ=="

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

        SBSLicense.setAppKey(appKey)
        FirebaseApp.configure()
        return true
    }
}
