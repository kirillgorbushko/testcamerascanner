//
//  CaptureSession+AuthRequest.swift
//
//
//  Created by Kirill Gorbushko on 10.01.18.
//  Copyright © 2018 - present . All rights reserved.
//

import UIKit
import AVFoundation

extension AVCaptureSession {

    // MARK: - AVCaptureSession+Auth

    class func checkPermissionForCamera(completion:@escaping ((_ granted: Bool) -> ())) {

        let askPermission:(() -> ()) = {
            AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { (granted: Bool) -> () in
                DispatchQueue.main.async {
                    completion(granted)
                }
            })
        }

        let authorizationStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        switch authorizationStatus {
            case .notDetermined:
                askPermission()
            case .authorized:
                DispatchQueue.main.async {
                    completion(true)
                }
            case .denied,
                 .restricted:
                DispatchQueue.main.async {
                    completion(false)
                }
            }
    }

    class func hasBackCamera() -> Bool {
        var hasBackCamera = false

        let devices = AVCaptureDevice.DiscoverySession(deviceTypes: [AVCaptureDevice.DeviceType.builtInWideAngleCamera],
                                                       mediaType: AVMediaType.video,
                                                       position: AVCaptureDevice.Position.back).devices
        hasBackCamera = !devices.isEmpty
        return hasBackCamera
    }
}
