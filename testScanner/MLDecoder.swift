//
//  MLDecoder.swift
//  testScanner
//
//  Created by Kirill Gorbushko on 17.07.18.
//  Copyright © 2018 - present. All rights reserved.
//

import Foundation
import FirebaseMLVision
import AVFoundation

//firebase

final class MLDecoder {

    /// Firebase vision instance.
    // [START init_vision]
    lazy var vision = Vision.vision()
    // [END init_vision]


    func processData(_ sampleBuffer: CMSampleBuffer, completion: ((String) -> ())? ) {

        guard CMSampleBufferGetImageBuffer(sampleBuffer) != nil else {
            print("Failed to get image buffer from sample buffer.")
            return
        }
        let visionImage = VisionImage(buffer: sampleBuffer)
        let metadata = VisionImageMetadata()
        let orientation = UIUtilities.imageOrientation(fromDevicePosition: .back)
        let visionOrientation = UIUtilities.visionImageOrientation(from: orientation)
        metadata.orientation = visionOrientation
        visionImage.metadata = metadata

        let format = VisionBarcodeFormat.all
        let barcodeOptions = VisionBarcodeDetectorOptions(formats: format)
        // [END config_barcode]

        // Create a barcode detector.
        // [START init_barcode]
        let barcodeDetector = vision.barcodeDetector(options: barcodeOptions)

        barcodeDetector.detect(in: visionImage) { features, error in
            guard error == nil, let features = features, !features.isEmpty else {
                // [START_EXCLUDE]
                // [END_EXCLUDE]
                return
            }

            // [START_EXCLUDE]
            let value =  features.map { feature in
                //                let transformedRect = feature.frame//.applying(self.transformMatrix())

                let value = "DisplayValue: \(feature.displayValue ?? ""), RawValue: " +
                "\(feature.rawValue ?? ""), Frame: \(feature.frame)"
                return value

                }
                .joined(separator: "\n")

            completion?(value)

            // [END_EXCLUDE
        }


        let image = imageFromSampleBuffer(sampleBuffer: sampleBuffer)
//        detectBarcodes(image: image, completion: completion )
    }

    /// Detects barcodes on the specified image and draws a frame around the detected barcodes using
    /// On-Device barcode API.
    ///
    /// - Parameter image: The image.
//    func detectBarcodes(image: UIImage?, completion: ((String) -> ())? ) {
//        guard let image = image else { return }
//
//        // Define the options for a barcode detector.
//        // [START config_barcode]
//        let format = VisionBarcodeFormat.all
//        let barcodeOptions = VisionBarcodeDetectorOptions(formats: format)
//        // [END config_barcode]
//
//        // Create a barcode detector.
//        // [START init_barcode]
//        let barcodeDetector = vision.barcodeDetector(options: barcodeOptions)
//        // [END init_barcode]
//
//        // Define the metadata for the image.
//        let imageMetadata = VisionImageMetadata()
//        imageMetadata.orientation = UIUtilities.visionImageOrientation(from: image.imageOrientation)
//
//        // Initialize a VisionImage object with the given UIImage.
//        let visionImage = VisionImage(image: image)
//        visionImage.metadata = imageMetadata
//
//        // [START detect_barcodes]
//        barcodeDetector.detect(in: visionImage) { features, error in
//            guard error == nil, let features = features, !features.isEmpty else {
//                // [START_EXCLUDE]
//                // [END_EXCLUDE]
//                return
//            }
//
//            // [START_EXCLUDE]
//           let value =  features.map { feature in
////                let transformedRect = feature.frame//.applying(self.transformMatrix())
//
//            let value = "DisplayValue: \(feature.displayValue ?? ""), RawValue: " +
//                "\(feature.rawValue ?? ""), Frame: \(feature.frame)"
//            return value
//
//                }
//            .joined(separator: "\n")
//
//            completion?(value)
//
//            // [END_EXCLUDE
//        }
//
//        // [END detect_barcodes]
//    }

    func imageFromSampleBuffer(sampleBuffer : CMSampleBuffer) -> UIImage
    {
        // Get a CMSampleBuffer's Core Video image buffer for the media data
        let  imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
        // Lock the base address of the pixel buffer
        CVPixelBufferLockBaseAddress(imageBuffer!, CVPixelBufferLockFlags.readOnly);


        // Get the number of bytes per row for the pixel buffer
        let baseAddress = CVPixelBufferGetBaseAddress(imageBuffer!);

        // Get the number of bytes per row for the pixel buffer
        let bytesPerRow = CVPixelBufferGetBytesPerRow(imageBuffer!);
        // Get the pixel buffer width and height
        let width = CVPixelBufferGetWidth(imageBuffer!);
        let height = CVPixelBufferGetHeight(imageBuffer!);

        // Create a device-dependent RGB color space
        let colorSpace = CGColorSpaceCreateDeviceRGB();

        // Create a bitmap graphics context with the sample buffer data
        var bitmapInfo: UInt32 = CGBitmapInfo.byteOrder32Little.rawValue
        bitmapInfo |= CGImageAlphaInfo.premultipliedFirst.rawValue & CGBitmapInfo.alphaInfoMask.rawValue
        //let bitmapInfo: UInt32 = CGBitmapInfo.alphaInfoMask.rawValue
        let context = CGContext.init(data: baseAddress, width: width, height: height, bitsPerComponent: 8, bytesPerRow: bytesPerRow, space: colorSpace, bitmapInfo: bitmapInfo)
        // Create a Quartz image from the pixel data in the bitmap graphics context
        let quartzImage = context?.makeImage();
        // Unlock the pixel buffer
        CVPixelBufferUnlockBaseAddress(imageBuffer!, CVPixelBufferLockFlags.readOnly);

        // Create an image object from the Quartz image
        let image = UIImage.init(cgImage: quartzImage!);

        return (image);
    }
}
