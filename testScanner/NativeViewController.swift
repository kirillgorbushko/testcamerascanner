//
//  ViewController.swift
//  testScanner
//
//  Created by Kirill Gorbushko on 04.07.18.
//  Copyright © 2018 - present. All rights reserved.
//

import UIKit

final class NativeViewController: UIViewController {

    @IBOutlet fileprivate weak var scanView: UIView!
    @IBOutlet fileprivate weak var resultLabel: UILabel!
    @IBOutlet fileprivate weak var clearButton: UIButton!
    @IBOutlet fileprivate weak var openSettingsButton: UIButton!

    @IBOutlet private weak var holderView: UIView!

    private var codeReader: CodeReader?

    override func viewDidLoad() {
        super.viewDidLoad()

        resultLabel.isHidden = true
        clearButton.isHidden = true
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        CodeReader.askPermission { (granted) in
            if granted {
                self.codeReader = CodeReader(self.scanView)
                self.codeReader?.delegate = self
                self.codeReader?.startStopReading()
                self.resultLabel.isHidden = false
            } else {
                DispatchQueue.main.async {
                    self.view.layoutIfNeeded()
                    UIView.animate(withDuration: 0.3, animations: {
                        self.resultLabel.text = "Permission for camera not granted :(\nSettings > Privacy > Camera"
                        self.openSettingsButton.isHidden = false
                        self.view.layoutIfNeeded()
                    })

                }
            }
        }
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        codeReader?.relayoutPreview()
    }

    @IBAction private func openSettingsButtonAction(_ sender: Any) {
        try? URLNavigator.open(PreferenceType.video)
    }

    @IBAction private func clearButtonAction(_ sender: Any) {
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.3, animations: {
            self.resultLabel.text = "Start scaning to see result..."
            self.clearButton.isHidden = true
            self.view.layoutIfNeeded()
        })
    }
}

extension NativeViewController: CodeReaderDelegate {

    // MARK: - CodeReaderDelegate

    func codeReaderDidFail(_ error: Error) {
        DispatchQueue.main.async {
            self.view.layoutIfNeeded()
            UIView.animate(withDuration: 0.3, animations: {
                self.resultLabel.text = "Error: " + "\n\n" + error.localizedDescription
                self.clearButton.isHidden = false
                self.view.layoutIfNeeded()
            })
        }
    }

    func codeReaderDidRecognizeData(_ recognizedValue: String) {
        DispatchQueue.main.async {
            self.view.layoutIfNeeded()
            UIView.animate(withDuration: 0.3, animations: {
                self.resultLabel.text = "Scanned at: " + Date().description + "\n\n" + recognizedValue
                self.clearButton.isHidden = false
                self.view.layoutIfNeeded()
            })
        }
    }
}
