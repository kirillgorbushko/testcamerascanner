//
//  ViewController.swift
//  MXSampleSwift
//
//  Created by Zhivko Manchev on 8/4/17.
//  Copyright © 2017 Cognex. All rights reserved.
//

import UIKit
//https://cmbdn.cognex.com/dashboard

class ViewController: UIViewController , CMBReaderDeviceDelegate {
    
    var readerDevice: CMBReaderDevice!
    var isScanning:Bool = false
    
    // MARK: UI OUTLETS
    @IBOutlet weak var btnScan: UIButton!
    @IBOutlet weak var ivPreview: UIImageView!
    @IBOutlet weak var lblCode: UILabel!
    @IBOutlet weak var lblSymbology: UILabel!
    @IBOutlet weak var lblConnection: UILabel!
    
    // MARK: VIEWCONTROLLER METHODS
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
        lblConnection.layer.cornerRadius = lblConnection.frame.size.height / 2
        lblConnection.clipsToBounds = true
        lblConnection.textColor = UIColor.white
        
        btnScan.backgroundColor = UIColor(red: 0.98, green: 0.86, blue: 0.01, alpha: 1.0)
        btnScan.tintColor = UIColor.black
        btnScan.layer.cornerRadius = 10
        btnScan.clipsToBounds = true
        btnScan.layer.borderColor = UIColor(red: 0.99, green: 0.73, blue: 0.07, alpha: 1.0).cgColor
        btnScan.layer.borderWidth = 1
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        updateUIByConnectionState()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        if readerDevice == nil {
            createReaderDevice()
        }
    }
    
    func createReaderDevice() {
        
        self.readerDevice?.disconnect()
        
            if let selectedDevice = (UserDefaults.standard.object(forKey: "selectedDevice") as? Int) {
                
                switch selectedDevice {
                    
                case 1:
                    self.readerDevice = CMBReaderDevice.readerOfDeviceCamera(with: CDMCameraMode.noAimer,
                                                                             previewOptions:CDMPreviewOption.init(rawValue: 0),
                                                                             previewView: self.ivPreview)
                case 2:
                    self.readerDevice = CMBReaderDevice.readerOfDeviceCamera(with: CDMCameraMode.passiveAimer,
                                                                             previewOptions:CDMPreviewOption.init(rawValue: 0),
                                                                             previewView: self.ivPreview)
                default:
                    self.readerDevice = CMBReaderDevice.readerOfMX()
                }
                
                self.readerDevice.delegate = self
                self.connectToReaderDevice() //try to connect
                self.updateUIByConnectionState()
            }
            else {
                self.selectDeviceShowingCancel(false)
            }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.readerDevice?.stopScanning()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        if (self.readerDevice != nil) && self.readerDevice!.connectionState == CMBConnectionStateConnected {
            self.readerDevice!.disconnect()
        }
    }
    
    // MARK: UPDATE UI
    func updateUIByConnectionState() {
        
        if self.readerDevice != nil && self.readerDevice.connectionState == CMBConnectionStateConnected{
            
            lblConnection.text = "  Connected  "
            lblConnection.backgroundColor = UIColor(red: 0.00, green: 0.39, blue: 0.00, alpha: 1.0)
            
            btnScan.setTitle("START SCANNING", for: .normal)
            btnScan.isEnabled = true
        }
        else{
            lblConnection.text = "  Disconnected  "
            lblConnection.backgroundColor = UIColor.red
            
            btnScan.setTitle("", for: .disabled)
            btnScan.isEnabled = false
        }
    }
    
    func clearResult() {
        
        print("clearResult begin")
        
        DispatchQueue.main.async(execute: { () -> Void in
            self.lblCode.text = nil;
            self.lblSymbology.text = nil;
            self.ivPreview.image = nil;
        })
        
        print("clearResult end")
    }
    
    // MARK: SELECT DEVICE
    @IBAction func deviceButtonTapped(_ sender: Any?) {
        
        selectDeviceShowingCancel(true)
    }
    
    func selectDeviceShowingCancel(_ showCancelOption: Bool) {
        
        let devicePicker = UIAlertController(title: "Select device", message: nil, preferredStyle: .actionSheet)
        
//        devicePicker.addAction(UIAlertAction(title: "MX Scanner", style: .default, handler: {(_ action: UIAlertAction) -> Void in
//            UserDefaults.standard.set(Int(0), forKey: "selectedDevice")
//            self.createReaderDevice()
//        }))
        devicePicker.addAction(UIAlertAction(title: "Mobile Camera", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            UserDefaults.standard.set(Int(1), forKey: "selectedDevice")
            self.createReaderDevice()
        }))
//        devicePicker.addAction(UIAlertAction(title: "Mobile Camera w/ basic Aimer", style: .default, handler: {(_ action: UIAlertAction) -> Void in
//            UserDefaults.standard.set(Int(2), forKey: "selectedDevice")
//            self.createReaderDevice()
//        }))
        if showCancelOption {
            devicePicker.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        }
        
        if devicePicker.popoverPresentationController != nil {
            devicePicker.popoverPresentationController!.barButtonItem = self.navigationItem.rightBarButtonItem;
        }
        
        present(devicePicker, animated: true, completion: nil)
    }
    
    // MARK: CONNECT - DISCONNECT
    func connectToReaderDevice(){
        
        if self.readerDevice.availability == CMBReaderAvailibilityAvailable && self.readerDevice.connectionState != CMBConnectionStateConnected {
            
            self.readerDevice.connect(completion: { (error:Error?) in
                
                if error != nil {
                    UIAlertView(title: "Failed to connect", message: error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK").show()
                }
            })
        }
    }
    
    func configureReaderDevice() {
        
        self.readerDevice.setSymbology(CMBSymbologyDataMatrix, enabled: true, completion: {(_ error: Error?) -> Void in
            if error != nil {
                print("FALIED TO ENABLE [Symbology_DataMatrix], \(error!.localizedDescription)")
            }
        })
        self.readerDevice.setSymbology(CMBSymbologyQR, enabled: true, completion: {(_ error: Error?) -> Void in
            if error != nil {
                print("FALIED TO ENABLE [Symbology_QR], \(error!.localizedDescription)")
            }
        })
        self.readerDevice.setSymbology(CMBSymbologyC128, enabled: true, completion: {(_ error: Error?) -> Void in
            if error != nil {
                print("FALIED TO ENABLE [Symbology_C128], \(error!.localizedDescription)")
            }
        })
        self.readerDevice.setSymbology(CMBSymbologyUpcEan, enabled: true, completion: {(_ error: Error?) -> Void in
            if error != nil {
                print("FALIED TO ENABLE [Symbology_UpcEan], \(error!.localizedDescription)")
            }
        })
        
        self.readerDevice.imageResultEnabled = true;
        self.readerDevice.dataManSystem().sendCommand("SET IMAGE.SIZE 0") // Used for better quality for the sample
        
        btnScan.setTitle("START SCANNING", for: .normal)
        isScanning = false
    }
    
    @IBAction func toggleScanner(_ sender: UIButton) {
        
        if isScanning {
            self.readerDevice?.stopScanning()
            sender.setTitle("START SCANNING", for: .normal)
        }
        else {
            self.readerDevice?.startScanning()
            sender.setTitle("STOP SCANNING", for: .normal)
        }
        
        isScanning = !isScanning
    }

    // MARK: MX Delegate methods
    func availabilityDidChange(ofReader reader: CMBReaderDevice) {
        
        print("DeviceSelectorVC availabilityDidChangeOfReader")
        print("readerAvailable: \(reader.availability == CMBReaderAvailibilityAvailable)")
        
        self.clearResult()
        
        if (reader.availability != CMBReaderAvailibilityAvailable) {
            
            UIAlertView(title: "Device became unavailable", message:nil, delegate: nil, cancelButtonTitle: "OK").show()
        }
        else {
            let selectedDevice = UserDefaults.standard.object(forKey: "selectedDevice") as? Int
            if selectedDevice == 0 {
                self.createReaderDevice()
            }
        }
    }
    
    func connectionStateDidChange(ofReader reader: CMBReaderDevice) {
        
        self.clearResult()
        
        if self.readerDevice.connectionState == CMBConnectionStateConnected {
            self.configureReaderDevice()
        }
        
        self.updateUIByConnectionState()
    }
    
    func didReceiveReadResult(fromReader reader: CMBReaderDevice, results readResults: CMBReadResults!) {
        
        print("didReceiveReadResult begin")
        
        self.clearResult()
        
        btnScan.setTitle("START SCANNING", for: .normal)
        isScanning = false
        
        if (readResults.readResults.count > 0){
            let firstResult: CMBReadResult? = readResults?.readResults[0] as? CMBReadResult
            
            DispatchQueue.main.async(execute: { () -> Void in
                self.ivPreview.image = firstResult?.image
                self.lblCode.text = firstResult?.readString
                self.lblSymbology.text = self.displayStringForSymbology(firstResult?.symbology)
            })
        }
        
        print("didReceiveReadResult end")
    }

    // MARK: UTILITY
    func displayStringForSymbology(_ symbology_in: CMBSymbology?) -> String?
    {
        if (symbology_in==nil){
            return nil;
        }
        else
        {
            let symbology = symbology_in!
            
            switch symbology {
                
            case CMBSymbologyDataMatrix:
                return "DATAMATRIX";
                
            case CMBSymbologyQR:
                return "QR";
                
            case CMBSymbologyC128:
                return "C128";
                
            case CMBSymbologyUpcEan:
                return "UPC-EAN";
                
            case CMBSymbologyC39:
                return "C39";
                
            case CMBSymbologyC93:
                return "C93";
                
            case CMBSymbologyC11:
                return "C11";
                
            case CMBSymbologyI2o5:
                return "I2O5";
                
            case CMBSymbologyCodaBar:
                return "CODABAR";
                
            case CMBSymbologyEanUcc:
                return "EAN-UCC";
                
            case CMBSymbologyPharmaCode:
                return "PHARMACODE";
                
            case CMBSymbologyMaxicode:
                return "MAXICODE";
                
            case CMBSymbologyPdf417:
                return "PDF417";
                
            case CMBSymbologyMicropdf417:
                return "MICROPDF417";
                
            case CMBSymbologyDatabar:
                return "DATABAR";
                
            case CMBSymbologyPostnet:
                return "POSTNET";
                
            case CMBSymbologyPlanet:
                return "PLANET";
                
            case CMBSymbologyFourStateJap:
                return "4STATE-JAP";
                
            case CMBSymbologyFourStateAus:
                return "4STATE-AUS";
                
            case CMBSymbologyFourStateUpu:
                return "4STATE-UPU";
                
            case CMBSymbologyFourStateImb:
                return "4STATE-IMB";
                
            case CMBSymbologyVericode:
                return "VERICODE";
                
            case CMBSymbologyRpc:
                return "RPC";
                
            case CMBSymbologyMsi:
                return "MSI";
                
            case CMBSymbologyAzteccode:
                return "AZTECCODE";
                
            case CMBSymbologyDotcode:
                return "DOTCODE";
                
            case CMBSymbologyC25:
                return "C25";
                
            case CMBSymbologyC39ConvertToC32:
                return "C39-CONVERT-TO-C32";
                
            case CMBSymbologyOcr:
                return "OCR";
                
            case CMBSymbologyFourStateRmc:
                return "4STATE-RMC";
                
            default:
                return nil;
                
            }
        }
    }
}



