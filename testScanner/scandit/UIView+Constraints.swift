//
//  UIView+Constraints.swift
//  testScanner
//
//  Created by Ivan.Pavliuk on 7/25/18.
//  Copyright © 2018 - present. All rights reserved.
//

import Foundation

extension UIView {
    
    func constrainToEdges(of superview: UIView, spacing: (top: CGFloat, bottom: CGFloat, left: CGFloat, right: CGFloat) = (0, 0, 0, 0)) {
        let pinTop = NSLayoutConstraint(item: self, attribute: .top, relatedBy: .equal,
                                        toItem: superview, attribute: .top, multiplier: 1.0, constant: spacing.top)
        let pinBottom = NSLayoutConstraint(item: self, attribute: .bottom, relatedBy: .equal,
                                           toItem: superview, attribute: .bottom, multiplier: 1.0, constant: spacing.bottom)
        let pinLeft = NSLayoutConstraint(item: self, attribute: .left, relatedBy: .equal,
                                         toItem: superview, attribute: .left, multiplier: 1.0, constant: spacing.left)
        let pinRight = NSLayoutConstraint(item: self, attribute: .right, relatedBy: .equal,
                                          toItem: superview, attribute: .right, multiplier: 1.0, constant: spacing.right)
        superview.addConstraints([pinTop, pinBottom, pinLeft, pinRight])
    }
    
    func constrainToCenter(of superview: UIView, widthMultiplier: CGFloat, heightMultiplier: CGFloat) {
        superview.addConstraint(NSLayoutConstraint(item: self, attribute: .centerX, relatedBy: .equal, toItem: superview, attribute: .centerX, multiplier: 1.0, constant: 0))
        superview.addConstraint(NSLayoutConstraint(item: self, attribute: .centerY, relatedBy: .equal, toItem: superview, attribute: .centerY, multiplier: 1.0, constant: 0))
        superview.addConstraint(NSLayoutConstraint(item: self, attribute: .width, relatedBy: .equal, toItem: superview, attribute: .width, multiplier: widthMultiplier, constant: 0))
        superview.addConstraint(NSLayoutConstraint(item: self, attribute: .height, relatedBy: .equal, toItem: superview, attribute: .height, multiplier: heightMultiplier, constant: 0))
    }
    
    func constrainToTop(of superview: UIView, widthMultiplier: CGFloat, heightMultiplier: CGFloat) {
        superview.addConstraint(NSLayoutConstraint(item: self, attribute: .centerX, relatedBy: .equal, toItem: superview, attribute: .centerX, multiplier: 1.0, constant: 0))
        superview.addConstraint(NSLayoutConstraint(item: self, attribute: .top, relatedBy: .equal, toItem: superview, attribute: .topMargin, multiplier: 1.0, constant: 0))
        superview.addConstraint(NSLayoutConstraint(item: self, attribute: .width, relatedBy: .equal, toItem: superview, attribute: .width, multiplier: widthMultiplier, constant: 0))
        superview.addConstraint(NSLayoutConstraint(item: self, attribute: .height, relatedBy: .equal, toItem: superview, attribute: .height, multiplier: heightMultiplier, constant: 0))
    }
}
