//
//  ScanditViewController.swift
//  testScanner
//
//  Created by Ivan.Pavliuk on 7/25/18.
//  Copyright © 2018 - present. All rights reserved.
//

import UIKit
import ScanditBarcodeScanner

class ScanditViewController: UIViewController {

    @IBOutlet fileprivate weak var scanView: UIView!
    @IBOutlet fileprivate weak var resultLabel: UILabel!
    @IBOutlet fileprivate weak var clearButton: UIButton!
    @IBOutlet fileprivate weak var openSettingsButton: UIButton!

    @IBOutlet private weak var holderView: UIView!


    override func viewDidLoad() {
        super.viewDidLoad()

        resultLabel.isHidden = true
        clearButton.isHidden = true
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        CodeReader.askPermission { (granted) in
            if granted {
                self.startScanningProcess()
                self.resultLabel.isHidden = false
            } else {
                DispatchQueue.main.async {
                    self.view.layoutIfNeeded()
                    UIView.animate(withDuration: 0.3, animations: {
                        self.resultLabel.text = "Permission for camera not granted :(\nSettings > Privacy > Camera"
                        self.openSettingsButton.isHidden = false
                        self.view.layoutIfNeeded()
                    })

                }
            }
        }
    }

    @IBAction private func openSettingsButtonAction(_ sender: Any) {
        try? URLNavigator.open(PreferenceType.video)
    }

    @IBAction private func clearButtonAction(_ sender: Any) {
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.3, animations: {
            self.resultLabel.text = "Start scaning to see result..."
            self.clearButton.isHidden = true
            self.view.layoutIfNeeded()
        })
    }
    
    private func startScanningProcess() {
        // Configure the barcode picker through a scan settings instance by defining which symbologies should be enabled.
        let scanSettings = SBSScanSettings.default()
        // prefer backward facing camera over front-facing cameras.
        scanSettings.cameraFacingPreference = .back
        // Enable symbologies that you want to scan
        scanSettings.setSymbology(.ean13, enabled: true)
        scanSettings.setSymbology(.upc12, enabled: true)
        scanSettings.setSymbology(.qr, enabled: true)
        scanSettings.setSymbology(.aztec, enabled: true)
        scanSettings.setSymbology(.codabar, enabled: true)
        scanSettings.setSymbology(.code11, enabled: true)
        scanSettings.setSymbology(.code128, enabled: true)
        scanSettings.setSymbology(.code25, enabled: true)
        scanSettings.setSymbology(.code39, enabled: true)
        scanSettings.setSymbology(.code93, enabled: true)
        scanSettings.setSymbology(.datamatrix, enabled: true)
        scanSettings.setSymbology(.dotCode, enabled: true)
        scanSettings.setSymbology(.ean8, enabled: true)
        scanSettings.setSymbology(.fiveDigitAddOn, enabled: true)
        scanSettings.setSymbology(.maxiCode, enabled: true)
        scanSettings.setSymbology(.upce, enabled: true)
        scanSettings.setSymbology(.twoDigitAddOn, enabled: true)
        scanSettings.setSymbology(.rm4scc, enabled: true)
        scanSettings.setSymbology(.microQR, enabled: true)
        scanSettings.setSymbology(.microPDF417, enabled: true)
        scanSettings.setSymbology(.kix, enabled: true)
        scanSettings.setSymbology(.itf, enabled: true)

        let picker = SBSBarcodePicker(settings: scanSettings)
        picker.willMove(toParentViewController: self)
        addChildViewController(picker)
        scanView.addSubview(picker.view)
        picker.didMove(toParentViewController: self)

        // Set the delegate to receive scan events.
        picker.scanDelegate = self
        // Start the scanning process.
        picker.startScanning()

    }
}

extension ScanditViewController: SBSScanDelegate {

    func barcodePicker(_ picker: SBSBarcodePicker, didScan session: SBSScanSession) {

        guard let code = session.newlyRecognizedCodes.first, let value = code.data else { return }
//        print("scanned \(code.symbologyName) barcode: \(code.data)")
        DispatchQueue.main.async {
            self.view.layoutIfNeeded()
            UIView.animate(withDuration: 0.3, animations: {
                self.resultLabel.text = "Scanned at: " + Date().description + "\n\n" + value
                self.clearButton.isHidden = false
                self.view.layoutIfNeeded()
            })
        }
    }

    func overlayController(_ overlayController: SBSOverlayController, didCancelWithStatus status: [AnyHashable : Any]?) {
        // Add your own code to handle the user canceling the barcode scan process
    }
}
