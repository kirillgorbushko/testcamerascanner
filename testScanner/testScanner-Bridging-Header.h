//
//  testScanner-Bridging-Header.h
//  testScanner
//
//  Created by Kirill Gorbushko on 18.07.18.
//  Copyright © 2018 - present. All rights reserved.
//

#ifndef testScanner_Bridging_Header_h
#define testScanner_Bridging_Header_h

    // MXReader
#import "CMBReaderDevice.h"
#import "CMBReadResult.h"
#import "CMBReadResults.h"

    // NeoReader
#import "NEOMCodeParser.h"
#import "NEOMGateway.h"
#import "NEOMLicenseRequestSettings.h"
#import "NEOMNeoReaderSDK.h"
#import "NEOMCodeParametersProtocol.h"
#import "NEOMViewfinderViewController.h"
#import "NEOMLicense.h"
#import "NEOMCode.h"

#endif /* testScanner_Bridging_Header_h */
