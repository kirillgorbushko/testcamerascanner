//
//  NEOMDecoder.h
//  NeoReaderSDK
//
//  Created by Martin Wermers on 04.07.11.
//  Copyright 2011 NeoMedia Europe GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class NEOMCode;
@class NEOMLicense;

typedef enum NEOMDecodingEngines_enum {
	NEOMDecodingEngines1D			= 1 << 0,
	NEOMDecodingEnginesDataMatrix	= 1 << 1,
	NEOMDecodingEnginesQR			= 1 << 2,
	NEOMDecodingEnginesAztec		= 1 << 3,
	NEOMDecodingEnginesPDF417		= 1 << 4,
	NEOMDecodingEnginesAll2D		= NEOMDecodingEnginesDataMatrix | NEOMDecodingEnginesQR | NEOMDecodingEnginesAztec | NEOMDecodingEnginesPDF417
} NEOMDecodingEngines;

typedef struct PixelBufferPoint {
	size_t x;
	size_t y;
} PixelBufferPoint;

static inline PixelBufferPoint PixelBufferPointMake(size_t x, size_t y) {
	return (PixelBufferPoint){x, y};
}

typedef struct PixelBufferSize {
	size_t width;
	size_t height;
} PixelBufferSize;

static inline PixelBufferSize PixelBufferSizeMake(size_t width, size_t height) {
	return (PixelBufferSize){width, height};
}

typedef struct PixelBufferRect {
	PixelBufferPoint origin;
	PixelBufferSize size;
} PixelBufferRect;

static inline PixelBufferRect PixelBufferRectMake(size_t x, size_t y, size_t width, size_t height) {
	return (PixelBufferRect){{x, y}, {width, height}};
}

/** 
 Implementation of the Decoder module. The class is used to provide access to the LavaSphere decoding engines.
 It offers several methods for decoding 8bit grayscale buffers or UIImage instances.

 There are several convenience methods to specify which engine to activate.
 However, if the convenience methods don't offer the desired combination of activated
 engines **don't call multiple convenience methods one after the other!**
 For performance reasons, please call `decodeBuffer:resolution:activatedEngines:` or
 `decodeUIImage:activatedEngines:` with the appropriate activatedEngines bitmask.
 
 You can set the bitmask by use of the `NEOMDecodingEngines` enum, which is defined as follows:
 
 	typedef enum NEOMDecodingEngines_enum {
 		NEOMDecodingEngines1D			=  1,
 		NEOMDecodingEnginesDataMatrix	=  2,
 		NEOMDecodingEnginesQR			=  4,
 		NEOMDecodingEnginesAztec		=  8,
 		NEOMDecodingEnginesPDF417		= 16,
 		NEOMDecodingEnginesAll2D		= NEOMDecodingEnginesDataMatrix | NEOMDecodingEnginesQR | NEOMDecodingEnginesAztec | NEOMDecodingEnginesPDF417
 	} NEOMDecodingEngines;
 */
@interface NEOMDecoder : NSObject

///---------------------------------------------------------------------------------------
/// @name Initialization & disposal
///---------------------------------------------------------------------------------------

/**  Constructs a Decoder instance for access to the LavaSphere decoding engines.
 
 @param aLicense 	NeoReader SDK customer's application identifier provided by NeoMedia
 @return 			Returns the initialized instance.
 @exception			NSInvalidArgumentException Thrown if the license parameter is `nil`.
 @exception			NEOMInsufficientLicenseException Thrown if the passed license is invalid.
*/
- (id)initWithLicense:(NEOMLicense *)aLicense;

///---------------------------------------------------------------------------------------
/// @name Decoding convenience methods
///---------------------------------------------------------------------------------------

/** Convenience method to decode an 8bit grayscale image using all engines unlocked in the license.

 @param grayscaleImageBuffer	8bit grayscale image.
 @param resolution 				Width and height of the image in pixels passed as a PixelBufferSize struct.
 @return NEOMCode object including code content and code type.
 @exception NEOMInsufficientLicenseException Thrown if the passed license is invalid.
 */
- (NEOMCode *)decodeAllFromBuffer:(unsigned char *)grayscaleImageBuffer withResolution:(PixelBufferSize)resolution;

/** Convenience method to decode an 8bit grayscale image using the 1D decoding engine.

 @param grayscaleImageBuffer	8bit grayscale image.
 @param resolution 				Width and height of the image in pixels passed as a PixelBufferSize struct.
 @return NEOMCode object including code content and code type.
 @exception NEOMInsufficientLicenseException Thrown if the passed license is invalid or if the 1D barcode engine is not unlocked in the license.
 */
- (NEOMCode *)decode1DFromBuffer:(unsigned char *)grayscaleImageBuffer withResolution:(PixelBufferSize)resolution;

/** Convenience method to decode an 8bit grayscale image using the 2D decoding (DM, QR, Aztec and PDF417) engine.

 @param grayscaleImageBuffer	8bit grayscale image.
 @param resolution 				Width and height of the image in pixels passed as a PixelBufferSize struct.
 @return NEOMCode object including code content and code type.
 @exception NEOMInsufficientLicenseException Thrown if the passed license is invalid or if one of the 2D barcode engines (Data Matrix, QR Code, Aztec or PDF417 engine) is not unlocked in the license.
 */
- (NEOMCode *)decode2DFromBuffer:(unsigned char *)grayscaleImageBuffer withResolution:(PixelBufferSize)resolution;

/** Convenience method to decode an 8bit grayscale image using the Aztec decoding engine.
 
 @param grayscaleImageBuffer	8bit grayscale image.
 @param resolution 				Width and height of the image in pixels passed as a PixelBufferSize struct.
 @return NEOMCode object including code content and code type.
 @exception NEOMInsufficientLicenseException Thrown if the passed license is invalid or if the Aztec code engine is not unlocked in the license.
 */
- (NEOMCode *)decodeAztecFromBuffer:(unsigned char *)grayscaleImageBuffer withResolution:(PixelBufferSize)resolution;

/** Convenience method to decode an 8bit grayscale image using the DataMatrix decoding engine.

 @param grayscaleImageBuffer	8bit grayscale image.
 @param resolution 				Width and height of the image in pixels passed as a PixelBufferSize struct.
 @return NEOMCode object including code content and code type.
 @exception NEOMInsufficientLicenseException Thrown if the passed license is invalid or if the Data Matrix engine is not unlocked in the license.
 */
- (NEOMCode *)decodeDataMatrixFromBuffer:(unsigned char *)grayscaleImageBuffer withResolution:(PixelBufferSize)resolution;

/** Convenience method to decode an 8bit grayscale image using the PDF417 decoding engine.
 
 @param grayscaleImageBuffer	8bit grayscale image.
 @param resolution 				Width and height of the image in pixels passed as a PixelBufferSize struct.
 @return NEOMCode object including code content and code type.
 @exception NEOMInsufficientLicenseException Thrown if the passed license is invalid or if the PDF417 engine is not unlocked in the license.
 */
- (NEOMCode *)decodePDF417FromBuffer:(unsigned char *)grayscaleImageBuffer withResolution:(PixelBufferSize)resolution;

/** Convenience method to decode an 8bit grayscale image using the QR decoding engine.

 @param grayscaleImageBuffer	8bit grayscale image.
 @param resolution 				Width and height of the image in pixels passed as a PixelBufferSize struct.
 @return NEOMCode object including code content and code type.
 @exception NEOMInsufficientLicenseException Thrown if the passed license is invalid or if the QR code engine is not unlocked in the license.
 */
- (NEOMCode *)decodeQRFromBuffer:(unsigned char *)grayscaleImageBuffer withResolution:(PixelBufferSize)resolution;

/** Convenience method to decode an UIImage object using all engines unlocked in the license.
 
 @param image	The UIImage instance to decode.
 @return NEOMCode object including code content and code type.
 @exception NEOMInsufficientLicenseException Thrown if the passed license is invalid.
 */
- (NEOMCode *)decodeAllFromUIImage:(UIImage *)image;

 /** Convenience method to decode an UIImage object using the 1D decoding engine.
 
 @param image	The UIImage instance to decode.
 @return NEOMCode object including code content and code type.
 @exception NEOMInsufficientLicenseException Thrown if the passed license is invalid or if the 1D barcode engine is not unlocked in the license.
 */
- (NEOMCode *)decode1DFromUIImage:(UIImage *)image;

/** Convenience method to decode an UIImage object using the 2D decoding (DM, QR, Aztec and PDF417) engine.
 
 @param image	The UIImage instance to decode.
 @return NEOMCode object including code content and code type.
 @exception NEOMInsufficientLicenseException Thrown if the passed license is invalid or if one of the 2D barcode engines (Data Matrix, QR Code, Aztec or PDF417 engine) is not unlocked in the license.
 */
- (NEOMCode *)decode2DFromUIImage:(UIImage *)image;

/** Convenience method to decode an UIImage object using the Aztec decoding engine.
 
 @param image	The UIImage instance to decode.
 @return NEOMCode object including code content and code type.
 @exception NEOMInsufficientLicenseException Thrown if the passed license is invalid or if the Aztec code engine is not unlocked in the license.
 */
- (NEOMCode *)decodeAztecFromUIImage:(UIImage *)image;

/** Convenience method to decode an UIImage object using the DataMatrix decoding engine.

 @param image	The UIImage instance to decode.
 @return NEOMCode object including code content and code type.
 @exception NEOMInsufficientLicenseException Thrown if the passed license is invalid or if the Data Matrix engine is not unlocked in the license.
 */
- (NEOMCode *)decodeDataMatrixFromUIImage:(UIImage *)image;

/** Convenience method to decode an UIImage object using the PDF417 decoding engine.
 
 @param image	The UIImage instance to decode.
 @return NEOMCode object including code content and code type.
 @exception NEOMInsufficientLicenseException Thrown if the passed license is invalid or if the PDF417 engine is not unlocked in the license.
 */
- (NEOMCode *)decodePDF417FromUIImage:(UIImage *)image;

/** Convenience method to decode an UIImage object using the QR decoding engine.
 
 @param image	The UIImage instance to decode.
 @return NEOMCode object including code content and code type.
 @exception NEOMInsufficientLicenseException Thrown if the passed license is invalid or if the QR code engine is not unlocked in the license.
 */
- (NEOMCode *)decodeQRFromUIImage:(UIImage *)image;

///---------------------------------------------------------------------------------------
/// @name Decoding methods with bitmask
///---------------------------------------------------------------------------------------

/** Decodes 8bit grayscale image using engines specified via bitmask.

 This method is called by the decodeFromBuffer convenience methods.
 If the convenience methods don't offer the desired combination of activated
 engines, call this method with the right bitmask. If you want for example to
 activate only the QR and the DataMatrix engine, pass (NEOMDecodingEnginesQR | NEOMDecodingEnginesDataMatrix)
 as value for the activatedEngines bitmask.
 
 @param grayscaleImageBuffer	8bit grayscale image.
 @param resolution 				Width and height of the image in pixels passed as a PixelBufferSize struct.
 @param activatedEngines		Bitmask of NEOMDecodingEngines enum values, that specifies which engines to use for decoding.
 @return NEOMCode object including code content and code type.
 @exception NEOMInsufficientLicenseException Thrown if the passed license is invalid or if one of the activated engines is not unlocked in the license.
 */
- (NEOMCode *)decodeBuffer:(unsigned char *)grayscaleImageBuffer resolution:(PixelBufferSize)resolution activatedEngines:(NEOMDecodingEngines)activatedEngines;

/** Decodes an UIImage object using engines specified via bitmask.
 
 This method is called by the decodeFromUIImage convenience methods.
 If the convenience methods don't offer the desired combination of activated
 engines, call this method with the right bitmask. If you want for example to
 activate only the 1D and the Aztec engine, pass (NEOMDecodingEngines1D | NEOMDecodingEnginesAztec)
 as value for the activatedEngines bitmask.

 @param image				The UIImage instance to decode.
 @param activatedEngines	Bitmask of NEOMDecodingEngines enum values, that specifies which engines to use for decoding.
 @return NEOMCode object including code content and code type.
 @exception NEOMInsufficientLicenseException Thrown if the passed license is invalid or if one of the activated engines is not unlocked in the license.
 */
- (NEOMCode *)decodeUIImage:(UIImage *)image activatedEngines:(NEOMDecodingEngines)activatedEngines;

///---------------------------------------------------------------------------------------
/// @name Decoding settings
///---------------------------------------------------------------------------------------

/** Changes the settings of the decoding engines.
 
 You can call this method to customize several parameters of the decoding engines.
 E.g. to enable additional 1D symbologies. Please refer to the documentation of
 [NEOMViewfinderViewController setDecodingParameters:] for a list of supported parameters and
 allowed values.
 
 @param parameters	An NSDictionary containing a list of parameters as pairs of parameter name and value.
 */
+ (void)setParameters:(NSDictionary *)parameters;

///---------------------------------------------------------------------------------------
/// @name Properties
///---------------------------------------------------------------------------------------

/** License that is used for decoding.
 
 Each time the license is changed, the new license will be checked for validity.
 */
@property (retain) NEOMLicense *license;

@end
