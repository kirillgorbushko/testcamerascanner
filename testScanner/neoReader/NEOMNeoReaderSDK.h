//
//  NEOMNeoReaderSDK.h
//  NeoReaderSDK
//
//  Created by Martin Wermers on 26.07.11.
//  Copyright 2011 NeoMedia Europe GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

#pragma mark -
#pragma mark Constants
extern NSString * const NEOMErrorDomain;
extern NSString * const kNEOMSDKVersion; 
extern NSString * const kNEOMProductName;

/**
 Helper class to get the SDK's version and product name.

 This class also defines the global variable `NEOMErrorDomain`, a constant that
 defines the Neomedia error domain.
	NSString * const NEOMErrorDomain;
 It's value is "com.neom.NeoReaderSDK.ErrorDomain".
 
 For example, NSError objects sent by the Viewfinder have the `NEOMErrorDomain`.
 The corresponding error codes are specified in the `NEOMViewfinderErrors` enum,
 defined in the NEOMViewfinderViewControllerDelegate protocol.
 
*/
@interface NEOMNeoReaderSDK : NSObject

/** Method to get the SDK's version.
 @return Returns the version string, e.g. "1.03.02"
*/
+ (NSString *)sdkVersion;

/** Method to get the SDK's product name.
 @return Returns the product name string. e.g. "iOSNeoReaderSDK"
*/
+ (NSString *)productName;

/** Method to query if the NeoReader SDK library is a debug build.
 @return Returns `YES` for debug builds, `NO` otherwise.
 */
+ (BOOL)isDebugBuild;

@end
