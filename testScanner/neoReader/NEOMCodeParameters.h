//
//  NEOMCodeParameters.h
//  NeoReaderSDK
//
//  Created by Martin Wermers on 21.07.11.
//  Copyright 2011 NeoMedia Europe GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NEOMCodeParametersProtocol.h"

/** An instance of the NEOMCodeParameters class or one of its subclasses
 is returned by [NEOMCodeParser parseCode:withLicense:].
 */

@interface NEOMCodeParameters : NSObject <NSCoding, NEOMCodeParametersProtocol>

///---------------------------------------------------------------------------------------
/// @name Initialization & disposal
///---------------------------------------------------------------------------------------

/** Initializes a newly allocated object with the strings provided in the parameters.

 @param aTitle 				Title string of the code.
 @param aContentString		Content string of the code.
 @return 					Returns the initialized instance.
 */
- (id)initWithTitle:(NSString *)aTitle content:(NSString *)aContentString;

///---------------------------------------------------------------------------------------
/// @name Properties
///---------------------------------------------------------------------------------------

/** Title string of the code. */
@property(nonatomic, copy, readonly) NSString *title;

/** Content string of the code. */
@property(nonatomic, copy, readonly) NSString *content;

/** Content type ID of the code. */
@property(nonatomic, assign, readonly) NEOMCodeParametersFormat format;

@end
