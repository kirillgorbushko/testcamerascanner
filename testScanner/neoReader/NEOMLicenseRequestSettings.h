//
//  NEOMLicenseRequestSettings.h
//  NeoReaderSDK
//
//  Created by Martin Wermers on 04.07.11.
//  Copyright 2011 NeoMedia Europe GmbH. All rights reserved.
//

#import <Foundation/NSObject.h>

/* NEOMLicense request settings keys. 
   The corresponding values in the dictionary for all keys defined below are
   of type BOOL, stored in an NSNumber object */

extern NSString * const kNEOM1DEngineLicenseRequestKey;			// corresponding value in dictionary is a BOOL - YES if license should unlock 1D decoding engines
extern NSString * const kNEOMAztecEngineLicenseRequestKey;		// corresponding value in dictionary is a BOOL - YES if license should unlock Aztec decoding engines
extern NSString * const kNEOMDMEngineLicenseRequestKey;			// corresponding value in dictionary is a BOOL - YES if license should unlock DataMatrix decoding engines
extern NSString * const kNEOMPDF417EngineLicenseRequestKey;		// corresponding value in dictionary is a BOOL - YES if license should unlock PDF417 decoding engines
extern NSString * const kNEOMQREngineLicenseRequestKey;			// corresponding value in dictionary is a BOOL - YES if license should unlock QR decoding engines
extern NSString * const kNEOMParserModuleLicenseRequestKey;		// corresponding value in dictionary is a BOOL - YES if license should unlock NEOMCodeParser
extern NSString * const kNEOMViewfinderModuleLicenseRequestKey;	// corresponding value in dictionary is a BOOL - YES if license should unlock ViewfinderViewController widget
extern NSString * const kNEOMBrandingOffLicenseRequestKey;		// corresponding value in dictionary is a BOOL - YES if license should unlock the 'Branding off' feature