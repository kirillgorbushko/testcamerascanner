//
//  NEOMOrganization.h
//  NeoReaderSDK
//
//  Created by Martin Wermers on 19.03.12.
//  Copyright (c) 2012 NeoMedia Europe GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AddressBook/AddressBook.h>

/**
 Objects of this class are referenced from within NEOMCardCodeParameters objects.
 */

@interface NEOMOrganization : NSObject

///---------------------------------------------------------------------------------------
/// @name Initialization & disposal
///---------------------------------------------------------------------------------------

/** Initializes a newly allocated object with the strings provided in the parameters.
 
 @param aName			Organization name part of the full organization information.
 @param theDepartments	String array of department and subdepartment names.
 @return 				Returns the initialized instance.
 */
- (id)initWithName:(NSString *)aName andDepartments:(NSArray *)theDepartments;

///---------------------------------------------------------------------------------------
/// @name Comparison
///---------------------------------------------------------------------------------------
/** Returns a Boolean value that indicates whether a given organization is equal to the
 receiver using a comparison of the organization name and the departments array.
 @param anOrganization	The organization with which to compare the receiver.
 @return 		`YES` if _anOrganization_ is equivalent to the receiver (if they have
 the same organization name and if their departments array are equal, otherwise `NO`.
 */
- (BOOL)isEqualToOrganization:(NEOMOrganization *)anOrganization;

///---------------------------------------------------------------------------------------
/// @name Helper Methods
///---------------------------------------------------------------------------------------

/** Set's both organization property values in the passed ABRecord instance.
 @param aRecord The address book record which will receive the receivers name strings.
 @return A boolean indicating if the setting of all name strings was successful.
 */
- (BOOL)setOrganizationFieldsInABRecord:(ABRecordRef)aRecord;

///---------------------------------------------------------------------------------------
/// @name Properties
///---------------------------------------------------------------------------------------

/** Organization name part found in a MeCard or vCard. */
@property(nonatomic, copy, readonly) NSString *name;

/** String array of department and subdepartment names found in a MeCard or vCard. */
@property(nonatomic, retain, readonly) NSArray  *departments;

@end
