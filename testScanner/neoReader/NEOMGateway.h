//
//  NEOMGateway.h
//  NeoReaderSDK
//
//  Created by Martin Wermers on 26.07.11.
//  Copyright 2011 NeoMedia Europe GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

@class NEOMCode;
@class CLLocation;

#pragma mark -
#pragma mark Constants
extern NSString * const kNEOMGatewayURL; 
extern NSString * const kNEOMGatewayIDCode;
extern NSString * const kNEOMGatewayIDSymb;
extern NSString * const kNEOMGatewayIDLang;
extern NSString * const kNEOMGatewayIDAge;
extern NSString * const kNEOMGatewayIDCtry;
extern NSString * const kNEOMGatewayIDGend;
extern NSString * const kNEOMGatewayIDLL;
extern NSString * const kNEOMGatewayIDHAcc;
extern NSString * const kNEOMGatewayIDAD;

extern NSString * const kNEOMGatewayIDLts;
extern NSString * const kNEOMGatewayIDCli;
extern NSString * const kNEOMGatewayIDAppID;
extern NSString * const kNEOMGatewayIDGUID;
extern NSString * const kNEOMGatewayIDBrand;
extern NSString * const kNEOMGatewaySDKBranding;

/**
 The Gateway class provides functionality to communicate with NeoMedia's
 NeoSphere backend servers with starting the webbrowser and being redirected
 to the information behind a code.
 */
@interface NEOMGateway : NSObject

///---------------------------------------------------------------------------------------
/// @name Initialization & disposal
///---------------------------------------------------------------------------------------

/**  Constructs a Gateway instance with the information needed by the NeoSphere backend servers

 @param anAppID 	NeoReader SDK customer's application identifier provided by NeoMedia
 @param aGUID 		Universally unique identifier which needs to be generated during the first application start.
 @return 			Returns the initialized instance.
 @exception			NSInvalidArgumentException Thrown if one of the passed parameters is `nil` or a passed string is empty.
*/
- (id)initWithApplicationID:(NSString *)anAppID GUID:(NSString *)aGUID;

/**  Constructs a Gateway object with the information needed by the NeoSphere backend servers

 @param anAppID 	NeoReader SDK customer's application identifier provided by NeoMedia
 @param aGUID 		Universally unique identifier which needs to be generated during the first application start.
 @param aLanguage 	Two-character standard ISO 639 language code. Optional parameter. May be `nil`.
 @param anAge 		Age of the end user in years. Optional parameter. May be `0`.
 @param aCountry 	Two-character standard ISO 3166 country code. Optional parameter. May be `nil`.
 @param aGender		One-character code (M or F) indicating the gender of the user. Optional parameter. May be `nil`.
 @return 			Returns the initialized instance.
 @exception			NSInvalidArgumentException Thrown if one of the mandatory parameters is `nil` or a mandatory string is empty.
*/
- (id)initWithApplicationID:(NSString *)anAppID GUID:(NSString *)aGUID language:(NSString *)aLanguage age:(NSUInteger)anAge country:(NSString *)aCountry gender:(NSString *)aGender;

///---------------------------------------------------------------------------------------
/// @name Handling
///---------------------------------------------------------------------------------------

/** This method creates an NSURL object which links to the NeoSphere backend, including all
 required and optionally set parameters (e.g. country, age, etc.). This URL can be used to
 send the code string of the passed code object to the NeoSphere backend via a self-configured
 UIWebView.
 You should use this method if you don't want to leave your app to display the result of the
 resolved NeoSphere request, which is what happens if you use the `sendCode:` method.
 @param aCode The NEOMCode object containing the code string that is going to be
 encoded in the URL to send the code to the NeoSphere backend.
 @return Returns an NSURL object with the URL to the NeoSphere backend for the passed code.
 */
- (NSURL *)neosphereURLForCode:(NEOMCode *)aCode;

/** Creates a NeoSphere request for the passed code and opens the request immediately with
 MobileSafari. Calling this method will thus cause your app to enter the background state.
 If you don't want your app to enter background state, call `neosphereURLForCode:`
 instead and open the returned NeoSphere URL with an UIWebView instance inside your app.
 @param aCode The NEOMCode object containing the code string that is sent to the NeoSphere backend.
*/
- (void)sendCode:(NEOMCode *)aCode;

///---------------------------------------------------------------------------------------
/// @name Properties
///---------------------------------------------------------------------------------------

/** Two-character standard ISO 639 language code. */
@property(nonatomic, copy) NSString *language;

/** Age of the end user in years. */
@property(nonatomic, assign) NSUInteger age;

/** Two-character standard ISO 3166 country code. */
@property(nonatomic, copy) NSString *country;

/** One-character code ("M" or "F") indicating the gender of the user. */
@property(nonatomic, copy) NSString *gender;

/** CLLocation object representing the location of the user. Latitude, longitude
    and horizontal accuracy are transmitted to the Gateway. */
@property(nonatomic, retain) CLLocation *location;

/** BOOL that specifies if the backend's webpage corresponding to the code's
 content should display advertisements or not.
 
 The default value for this property is `NO`.
*/
@property(nonatomic, assign, getter=areAdsEnabled) BOOL ads;

@end
