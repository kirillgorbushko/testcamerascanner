//
//  NEOMCodeParser.h
//  NeoReaderSDK
//
//  Created by Martin Wermers on 19.07.11.
//  Copyright 2011 NeoMedia Europe GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

@class NEOMLicense;
@class NEOMCode;
@class NEOMCodeParameters;
@protocol NEOMCodeParametersProtocol;

/** Implementation of the Parser module.

 The Parser module analyses the barcode and converts it into a more
 usable format which means one of the classes derived from NEOMCodeParameters.
 */
@interface NEOMCodeParser : NSObject

/** Parses the String representation of the NEOMCode object. 

 First, the code is split into title and content if the code contains one of
 the following white space characters (CR, LF, CRLF, BS, HT) before the content
 identifier (e.g. "http://"). Secondly the results of further breaking down the
 content part is used to create the appropriate NEOMCodeParameters object which
 is then returned and may be stored in the code object as well.
 @param aCode The NEOMCode object to parse.
 @param aLicense A valid NEOMLicense instance which has the _Parser module_ unlocked. Regarding validity of a license, cf. to [NEOMLicense isValid].
 @return NEOMCodeParameters object that is either of type NEOMEmailCodeParameters, 
 NEOMCallCodeParameters, NEOMSMSCodeParameters, NEOMWebCodeParameters, or 
 NEOMAppstoreCodeParameters 
 @exception NSInvalidArgumentException Thrown if one of the passed parameters is `nil`.
 @exception NEOMInsufficientLicenseException Thrown if the passed license is invalid or if the Parser module is not unlocked in the license.
 */
+ (NEOMCodeParameters *)parseCode:(NEOMCode *)aCode withLicense:(NEOMLicense *)aLicense;

@end
