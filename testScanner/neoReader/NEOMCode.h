//
//  NEOMCode.h
//  NeoReaderSDK
//
//  Created by Martin Wermers on 04.07.11.
//  Copyright 2011 NeoMedia Europe GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NEOMCodeParameters.h"

@class NEOMScanResultInfo;

/**
 Class that provides code content, code type etc.

 Description of the NEOMCodeType enum:
	typedef enum {
		kERR = -1,
		kNO_CODE = 0,
		kUNKNOWN_CODE = 1,
		kUPC_A = 2,
		kUPC_E = 3,
		kEAN_13 = 4,
		kEAN_8 = 5,
		kJAN_13 = 6,
		kJAN_8 = 7,
		kCODE_11 = 8,
		kCODE_39 = 9,
		kCODE_93 = 10,
		kCODE_128 = 11,
		kCODE_INTERLEAVED = 12,
		kCODE_CODABAR = 13,
		kCODE_SCANLET = 14,
		kCODE_CHINESE_POST_CODE = 15,
		kDM = 16,
		kQR = 17,
		kAZTEC = 18,
		kOCR = 19,
		kPDF417 = 20,
		kUPC = 91,
		kEAN = 92,
		kMANUAL_CODE = 99
	} NEOMCodeType;
 */

typedef enum NEOMCodeType_enum {
	kERR = -1,
	kNO_CODE = 0,
	kUNKNOWN_CODE = 1,
	kUPC_A = 2,
	kUPC_E = 3,
	kEAN_13 = 4,
	kEAN_8 = 5,
	kJAN_13 = 6,
	kJAN_8 = 7,
	kCODE_11 = 8,
	kCODE_39 = 9,
	kCODE_93 = 10,
	kCODE_128 = 11,
	kCODE_INTERLEAVED = 12,
	kCODE_CODABAR = 13,
	kCODE_SCANLET = 14,
	kCODE_CHINESE_POST_CODE = 15,
	kDM = 16,
	kQR = 17,
	kAZTEC = 18,
	kOCR = 19,
	kPDF417 = 20,
	kUPC = 91,
	kEAN = 92,
	kMANUAL_CODE = 99
} NEOMCodeType;


@interface NEOMCode : NSObject <NSCoding>

///---------------------------------------------------------------------------------------
/// @name Initialization & disposal
///---------------------------------------------------------------------------------------

/** Constructor that should be used for manually entered codes
 Allocates and initializes an autoreleased object with the provided parameters.

 @param aCodeString		The code content entered manually.
 @return 				Returns an autorelease instance.
*/
+ (instancetype)codeWithString:(NSString *)aCodeString;

/** Constructor that is used by the internal decoder.
 Allocates and initializes an autoreleased object with the provided parameters.
 @deprecated			Use method codeWithData:type:resultInfo: instead.
 @param aCodeString		The code content as string.
 @param aCodeType		The NEOMCodeType of the decoded code.
 @param resultInfo		Additional scanning information returned by LavaSphere.
 @return 				Returns an autorelease instance.
 */
+ (instancetype)codeWithString:(NSString *)aCodeString type:(NEOMCodeType)aCodeType resultInfo:(NEOMScanResultInfo *)resultInfo __deprecated;

/** Constructor that is actually used by the internal decoder.
 Allocates and initializes an autoreleased object with the provided parameters.
 @param aCodeContent	The code content as it was decoded.
 @param aCodeType		The NEOMCodeType of the decoded code.
 @param resultInfo		Additional scanning information returned by LavaSphere.
 @return 				Returns an autorelease instance.
 */
+ (instancetype)codeWithData:(NSData *)aCodeContent type:(NEOMCodeType)aCodeType resultInfo:(NEOMScanResultInfo *)resultInfo;

/** Constructor that should be used for manually entered codes
 Initializes a newly allocated object with the provided parameters.

 @param aCodeString		The code content entered manually.
 @return 				Returns the initialized instance.
*/
- (instancetype)initWithString:(NSString *)aCodeString;

/** Constructor that is used by the internal decoder.
 Initializes a newly allocated object with the provided parameters.
 @deprecated			Use method initWithData:type:resultInfo: instead.
 @param aCodeString		The code content as string.
 @param aCodeType		The NEOMCodeType of the decoded code.
 @param resultInfo		Additional scanning information returned by LavaSphere.
 @return 				Returns the initialized instance.
 */
- (instancetype)initWithString:(NSString *)aCodeString type:(NEOMCodeType)aCodeType resultInfo:(NEOMScanResultInfo *)resultInfo  __deprecated;

/** Constructor that is actually used by the internal decoder.
 Initializes a newly allocated object with the provided parameters.
 @param aCodeContent	The code content as it was decoded.
 @param aCodeType		The NEOMCodeType of the decoded code.
 @param resultInfo		Additional scanning information returned by LavaSphere.
 @return 				Returns the initialized instance.
 */
- (instancetype)initWithData:(NSData *)aCodeContent type:(NEOMCodeType)aCodeType resultInfo:(NEOMScanResultInfo *)resultInfo;

///---------------------------------------------------------------------------------------
/// @name Comparison
///---------------------------------------------------------------------------------------
/** Returns a Boolean value that indicates whether a given code is equal to the
 receiver using a comparison of the codeContent and the codeType properties.
 @param aCode	The code with which to compare the receiver.
 @return 		`YES` if _aCode_ is equivalent to the receiver (if they have
 the same codeType and if their codeContent properties contain the same data), otherwise `NO`.
 */
- (BOOL)isEqualToCode:(NEOMCode *)aCode;

///---------------------------------------------------------------------------------------
/// @name Helper
///---------------------------------------------------------------------------------------

/** Creates a human readable string corresponding to any provided code Type
 @param aCodeType The NEOMCodeType to create the human readable string.
 @return A human readable string of the code type.
*/
+ (NSString *)stringFromCodeType:(NEOMCodeType)aCodeType;

/** Parses the string passed as parameter for a human readable string
 corresponding to a NEOMCodeType. Inverse method of `stringFromCodeType`.
 @param aCodeTypeString The string object holding the human readable string.
 @return The NEOMCodeType corresponding to the human readable codeType string,
 or kUNKNOWN_CODE if the string was not recognized as a valid code type.
 */
+ (NEOMCodeType)codeTypeFromString:(NSString *)aCodeTypeString;

/** Returns a human readable string of the receivers code type.
 @return A human readable string of the code type.
*/
- (NSString *)codeTypeString;

///---------------------------------------------------------------------------------------
/// @name Properties
///---------------------------------------------------------------------------------------

/** The code content as it was decoded. */
@property(copy, readonly) NSData *codeContent;

/** The code content as string. UTF-8 encoding is used for the conversion. If the code
 content is not a valid UTF-8 string it is interpreted as Latin 1 encoded instead. */
@property(copy, readonly) NSString *codeString;

/** A property to optionally store a custom code title. */
@property(copy) NSString *userTitle;

/** A NSDate object refering to the date and time the code was decoded. */
@property(retain) NSDate *creationDate;

/** The NEOMCodeType of the decoded code. */
@property(assign, readonly) NEOMCodeType codeType;

/** A NSString representation for the NEOMCodeType of the decoded code. */
@property(copy, readonly) NSString *codeTypeString;

/** Placeholder variable to opt. store the code parameters object returned by the
 [Parser module's](NEOMCodeParser) parseCode:withLicense: method. */
@property(retain) NEOMCodeParameters *codeParameters;

@property(retain, readonly) NEOMScanResultInfo *scanResultInfo;


@end
