//
//  NEOMEmailCodeParameters.h
//  NeoReaderSDK
//
//  Created by Martin Wermers on 21.07.11.
//  Copyright 2011 NeoMedia Europe GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NEOMCodeParameters.h"

/**
 An object of this class is returned by [NEOMCodeParser parseCode:withLicense:]
 if the parsed code is an email-code. 
 */
@interface NEOMEmailCodeParameters : NEOMCodeParameters <NEOMCodeParametersProtocol>

///---------------------------------------------------------------------------------------
/// @name Initialization & disposal
///---------------------------------------------------------------------------------------

/** Initializes a newly allocated object with the strings provided in the parameters.
 @param aTitle 				Title string of the code.
 @param aContentString		Content string of the code.
 @param theTOArray			Email addresses of the  to-Recipients found in the code's content part, stored as NSStrings in an NSArray.
 @param theCCArray			Email addresses of the  cc-Recipients found in the code's content part, stored as NSStrings in an NSArray.
 @param theBCCArray			Email addresses of the bcc-Recipients found in the code's content part, stored as NSStrings in an NSArray.
 @param aSubject			Email's subject found in the code's content part.
 @param aBody				Email message body found in the code's content part.
 @return 					Returns the initialized instance.
 */
- (id)initWithTitle:(NSString *)aTitle content:(NSString *)aContentString toRecipients:(NSArray *)theTOArray ccRecipients:(NSArray *)theCCArray bccRecipients:(NSArray *)theBCCArray subject:(NSString *)aSubject body:(NSString *)aBody;

///---------------------------------------------------------------------------------------
/// @name Helper Methods
///---------------------------------------------------------------------------------------
/** A convenience method to get a URI representation of the email parameters.
 @return 					The URI string representation.
 */
- (NSString *)URIStringRepresentation;

///---------------------------------------------------------------------------------------
/// @name Properties
///---------------------------------------------------------------------------------------

/** Email addresses of the recipients found in the code's 'to' field. */
@property(nonatomic, retain, readonly) NSArray *to;

/** Email addresses of the recipients found in the code's 'cc' field. */
@property(nonatomic, retain, readonly) NSArray *cc;

/** Email addresses of the recipients found in the code's 'bcc' field. */
@property(nonatomic, retain, readonly) NSArray *bcc;

/** Email's subject found in the code's content part. */
@property(nonatomic, copy, readonly) NSString *subject;

/** Email message body found in the code's content part. */
@property(nonatomic, copy, readonly) NSString *body;

@end
