//
//  NEOMCallCodeParameters.h
//  NeoReaderSDK
//
//  Created by Martin Wermers on 21.07.11.
//  Copyright 2011 NeoMedia Europe GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NEOMCodeParameters.h"

/**
 An object of this class is returned by [NEOMCodeParser parseCode:withLicense:]
 if the parsed code is a phone call-code. 
 */
@interface NEOMCallCodeParameters : NEOMCodeParameters <NEOMCodeParametersProtocol>

///---------------------------------------------------------------------------------------
/// @name Initialization & disposal
///---------------------------------------------------------------------------------------

/** Initializes a newly allocated object with the strings provided in the parameters.
 
 @param aTitle 				Title string of the code.
 @param aContentString		Content string of the code.
 @param aPhoneNumber		Phone number found in the code's content part.
 @return 					Returns the initialized instance.
 */
- (id)initWithTitle:(NSString *)aTitle content:(NSString *)aContentString phoneNumber:(NSString *)aPhoneNumber;

///---------------------------------------------------------------------------------------
/// @name Properties
///---------------------------------------------------------------------------------------

/** Phone number found in the code.

*Example:* If the code includes tel:+49221123456, the phoneNumber is +49221123456.
*/
@property(nonatomic, copy, readonly) NSString *phoneNumber;

@end
