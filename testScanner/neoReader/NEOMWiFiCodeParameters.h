//
//  NEOMWiFiCodeParameters.h
//  NeoReaderSDK
//
//  Created by Martin Wermers on 05.03.12.
//  Copyright 2012 NeoMedia Europe GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NEOMCodeParameters.h"

typedef enum NEOMWiFiAuthenticationType_enum {
	kWIFI_UNKNOWN_AUTHENTICATION,
	    kWIFI_WEP_AUTHENTICATION,
	    kWIFI_WPA_AUTHENTICATION
} NEOMWiFiAuthenticationType;

/**
 An object of this class is returned by [NEOMCodeParser parseCode:withLicense:]
 if the parsed code is a WiFi-code.
 */
@interface NEOMWiFiCodeParameters : NEOMCodeParameters <NEOMCodeParametersProtocol>

///---------------------------------------------------------------------------------------
/// @name Initialization & disposal
///---------------------------------------------------------------------------------------

/** Initializes a newly allocated object with the provided parameters.
 
 @param aTitle 					Title string of the code.
 @param aContentString			Content string of the code.
 @param aSSID					The Service Set Identifier (SSID) found in a WiFi code's content part.
 @param aPassword				The WiFi password found in a code's content part.
 @param anAuthenticationType	The WiFi authentication type found in a code's content part.
 @return 						Returns the initialized instance.
 */
- (id)initWithTitle:(NSString *)aTitle content:(NSString *)aContentString SSID:(NSString *)aSSID password:(NSString *)aPassword type:(NEOMWiFiAuthenticationType)anAuthenticationType;


///---------------------------------------------------------------------------------------
/// @name Properties
///---------------------------------------------------------------------------------------

/** The Service Set Identifier (SSID) found in the WiFi code's content part. */
@property(nonatomic, copy, readonly) NSString *SSID;

/** The WiFi password found in the code's content part. */
@property(nonatomic, copy, readonly) NSString *password;

/** The WiFi authentication type found in the code's content part. */
@property(nonatomic, assign, readonly) NEOMWiFiAuthenticationType type;

@end
