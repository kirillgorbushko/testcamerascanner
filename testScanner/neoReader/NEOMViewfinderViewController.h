//
//  NEOMViewfinderViewController.h
//  NeoReaderSDK
//
//  Created by Martin Wermers on 04.07.11.
//  Copyright 2011 NeoMedia Europe GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <CoreVideo/CoreVideo.h>
#include "TargetConditionals.h"

//
// NOTE: When modifying the list of engine parameter defines, rember to
// update the documentation of the method 'setDecodingParameters:' as well!
//
#define kNEOMDecodingParameter1DEANEnable @"BC.EAN.Enable"
#define kNEOMDecodingParameter1DEANSupplementals @"BC.EAN.Supplementals"

#define kNEOMDecodingParameter1DITFEnable @"BC.ITF.Enable"
#define kNEOMDecodingParameter1DITFWithCheckchar @"BC.ITF.WithCheckchar"
#define kNEOMDecodingParameter1DITFCodesize @"BC.ITF.Codesize"

#define kNEOMDecodingParameter1DCode39Enable @"BC.Code39.Enable"
#define kNEOMDecodingParameter1DCode39WithCheckchar @"BC.Code39.WithCheckchar"
#define kNEOMDecodingParameter1DCode39Extended @"BC.Code39.Extended"
#define kNEOMDecodingParameter1DCode39Codesize @"BC.Code39.Codesize"

#define kNEOMDecodingParameter1DCode93Enable @"BC.Code93.Enable"

#define kNEOMDecodingParameter1DCode128Enable @"BC.Code128.Enable"
#define kNEOMDecodingParameter1DCode128Codesize @"BC.Code128.Codesize"

#define kNEOMDecodingParameter1DCodabarEnable @"BC.Codabar.Enable"

#define kNEOMDecodingParameter1DCode11Enable @"BC.Code11.Enable"
#define kNEOMDecodingParameter1DCode11TwoCheckchars @"BC.Code11.2Checkchar"

#define kNEOMDecodingParameter1DChinesePostCodeEnable @"BC.ChinesePostCode.Enable"

#define kNEOMDecodingParameter1DDecodingStrategy @"BC.DecodingStrategy"

typedef struct NEOMDecodingRectSize {
	CGFloat longEdge;
	CGFloat shortEdge;
} NEOMDecodingRectangleSize;

static inline NEOMDecodingRectangleSize NEOMDecodingRectSizeMake(CGFloat longEdge, CGFloat shortEdge) {
	return (NEOMDecodingRectangleSize){longEdge, shortEdge};
}

@protocol NEOMViewfinderViewControllerDelegate;
@class NEOMCode;
@class NEOMLicense;

/**
 This is the NeoReaderSDK Viewfinder View. Due to the Livestream functions, the
 minimum required iOS version is 4.0.
 When integrating this UI widget into own layouts, the livestream starts
 automatically with a resolution of 640x480 on most devices (400x304 on iPhone 3G).
 The decoding can be started by calling startLivestreamDecoding with a valid
 NEOMLicense object.

 The class that initializes the NEOMViewfinderViewController instance should set
 a delegate that conforms to the NEOMViewfinderViewControllerDelegate protocol,
 in order to receive messages about the decoding result of livestream and
 snapshot images, as well as to be notified in error cases.
 
 After the Viewfinder's view was loaded, the methods `openCamera` and `startLivestream`
 are called automatically from within the NEOMViewfinderViewController's
 `viewDidLoad:` method, so that you will see a live camera preview on the screen.
 You may then call the methods `startLivestreamDecodingWithLicense:` or 
 `decodeSnapshotWithLicense:`.

 **Important:**
 If you embed the Viewfinder into your own Viewcontroller you should notice
 that the Viewfinders implementation of `viewWillAppear` and `viewDidAppear`
 is not called by the system in this case. Similarly, the methods `viewWillDisappear`
 and `viewDidDisappear` are only called automatically from iOS 5 on.
 As the Viewfinder stops the Livestream in its `viewDidDisappear` implementation,
 and restarts it in `viewDidAppear`, you have to call these methods manually
 in case you have embedded the Viewfinder's view in your own Viewcontroller.
 
 If you need finer control of the camera, you can use the six methods listed in
 the 'Camera & Decoding control' section. Please notice however that you have to
 call these six methods in the correct sequence, which is as follows:
 
 When initializing the camera:
	// get reference to initialized NEOMViewfinderViewController
	NEOMViewfinderViewController *viewfinderViewController = ... 
	
	[viewfinderViewController openCamera];
	[viewfinderViewController startLivestream];
	[viewfinderViewController startLivestreamDecodingWithLicense:aValidLicense];
 
 When releasing the camera:
	[viewfinderViewController stopLivestreamDecoding];
	[viewfinderViewController stopLivestream];
	[viewfinderViewController releaseCamera];

 Of course it is not mandatory to call all three methods of each block, and of
 course you may distribute the calls among different methods.
 This sequence rather implies the following: 
  	- Don't call 'startLivestream' before 'openCamera' was called.
  	- Don't call 'startLivestreamDecodingWithLicense:' before 'openCamera' was called.
  	- Don't call 'startLivestreamDecodingWithLicense:' before 'startLivestream' was called.

 
**Camera Selection:**

 To select between the front and back camera, use `setCameraPosition:` and pass a value
 of the `AVCaptureDevicePosition` enum (e.g. `AVCaptureDevicePositionBack` or `AVCaptureDevicePositionFront`).
 Make sure to check if the device has a camera at the desired position by calling the `isCameraAvailableAtPosition:` method beforehand.
 NEOMViewfinderViewController will use the default camera of the device on its first start.
 On devices with two cameras (back and front) this is typically the camera at the back of the device.
 You can check the position of the active camera by calling `cameraPosition`.
 */

@interface NEOMViewfinderViewController : UIViewController

///---------------------------------------------------------------------------------------
/// @name Camera & Decoding control 
///---------------------------------------------------------------------------------------

/** Initializes and configures the device's back camera for processing live images and taking snapshots.

This method is automatically called from within the `viewDidLoad:` message of the NEOMViewfinderViewController. 
Usually it should not be necessarry to call this method, but in case of memory constraints it is possible to manually release and later reopen the camera.
@return Returns `YES` if initialization of the camera was successful. In case of an error, `NO` is returned and the [NEOMViewfinderViewControllerDelegate viewfinder:encounteredError:] message is called with a corresponding error code from `NEOMErrorDomain`.
 */
- (BOOL)openCamera;

/** Clean's up all internal objects of the camera subsystem.

This method is automatically called from within the `viewDidDisappear:` message of the NEOMViewfinderViewController. 
Usually it should not be necessarry to call this method, but in case of memory constraints it is possible to manually release and later reopen the camera.
 */
- (void)releaseCamera;

/** Initializes a livestream preview layer and add's it as a sublayer to the Viewfinder's view.

This method is automatically called from within the `viewDidLoad:` message of the NEOMViewfinderViewController. 
Usually it should not be necessarry to call this method, but in case of memory constraints it is possible to manually start and stop the livestream.
@return Returns `YES` if initialization of the camera was successful. In case of an error, `NO` is returned and the [NEOMViewfinderViewControllerDelegate viewfinder:encounteredError:] message is called with a corresponding error code from `NEOMErrorDomain`.
 */
- (BOOL)startLivestream;

/** Stops the livestream by removing the livestream preview layer from the Viewfinder's view.

This method is automatically called from within the releaseCamera: message. 
Usually it should not be necessarry to call this method, but in case of memory constraints it is possible to manually start and stop the livestream.
 */
- (void)stopLivestream;


/** Method to activate livestream decoding.

 Call this method to actually send all livestream images to the internal decoder.
   Implement [NEOMViewfinderViewControllerDelegate viewfinder:didDecodeLivestreamWithResult:] and
   [NEOMViewfinderViewControllerDelegate viewfinderFailedToDecodeLivestream:] to receive messages about decoding results.
 @param license A valid NEOMLicense instance which has the _Viewfinder module_ unlocked. Regarding validity of a license, cf. to [NEOMLicense isValid].
 @return Returns `YES` if initialization of the camera was successful. In case of an error, `NO` is returned and the [NEOMViewfinderViewControllerDelegate viewfinder:encounteredError:] message is called with a corresponding error code from `NEOMErrorDomain`.
 @exception NSInvalidArgumentException Thrown if the passed license is nil.
 @exception NEOMInsufficientLicenseException Thrown if the passed license is invalid or if the Viewfinder module is not unlocked in the license.
*/
- (BOOL)startLivestreamDecodingWithLicense:(NEOMLicense *)license;

/** Method to deactivate livestream decoding.

Call this method to stop livestream decoding, e.g. after a code was found to display or process the code's content.
 */
- (void)stopLivestreamDecoding;

/** Method for snapshot decoding.

 Call this method to take a snapshot with the camera and to send it to the internal decoder. 
   Implement [NEOMViewfinderViewControllerDelegate viewfinder: didDecodeSnapshotWithResult:] and [NEOMViewfinderViewControllerDelegate viewfinderFailedToDecodeSnapshot:] to receive messages about decoding results.
 @param license A valid NEOMLicense instance which has the _Viewfinder module_ unlocked. Regarding validity of a license, cf. to [NEOMLicense isValid].
 @return Returns `YES` if initialization of the camera was successful. In case of an error, `NO` is returned and the [NEOMViewfinderViewControllerDelegate viewfinder:encounteredError:] message is called with a corresponding error code from `NEOMErrorDomain`.
 @exception NSInvalidArgumentException Thrown if the passed license is nil.
 @exception NEOMInsufficientLicenseException Thrown if the passed license is invalid or if the Viewfinder module is not unlocked in the license.
*/
- (BOOL)decodeSnapshotWithLicense:(NEOMLicense *)license;

/** Changes the settings of the 1D decoding engine.
 
 You can call this method to customize several parameters of the 1D decoding engine.
 E.g. to enable additional 1D symbologies.
 
 The following parameters are supported:
 
 - kNEOMDecodingParameter1DEANEnable  
   **(BOOL)**  
   *Enable EAN/UPC/JAN symbology.*  
   *Default: YES*
 
 - kNEOMDecodingParameter1DEANSupplementals  
   **(Integer: 0 - 4)**  
   *0: Ignore supplementals*  
   *1: Supplemental required*  
   *2: 2-digit supplemental required*  
   *3: 5-digit supplemental required*  
   *4: Supplemental optional*  
   *Default: 0*
 
 - kNEOMDecodingParameter1DITFEnable  
   **(BOOL)**  
   *Enable Interleaved 2 of 5 symbology.*  
   *Default: NO*
 
 - kNEOMDecodingParameter1DITFWithCheckchar  
   **(BOOL)**  
   *Check digit required for Interleaved 2 of 5 codes.*  
   *Default: NO*
 
 - kNEOMDecodingParameter1DITFCodesize  
   **(Integer: 0 - 255)**  
   *Required length of Interleaved 2 of 5 codes. The value is given as the required length divided by two. 0 means any length.*  
   *Default: 0*
 
 - kNEOMDecodingParameter1DCode39Enable  
   **(BOOL)**  
   *Enable Code 39 symbology.*  
   *Default: YES*
 
 - kNEOMDecodingParameter1DCode39WithCheckchar  
   **(BOOL)**  
   *Check digit required for Code 39 codes.*  
   *Default: NO*

 - kNEOMDecodingParameter1DCode39Extended  
   **(BOOL)**  
   *Enable Code 39 Extended mode.*  
   *Default: YES*
 
 - kNEOMDecodingParameter1DCode39Codesize  
   **(Integer: 0 - 255)**  
   *Required length of Code 39 codes. 0 means any length.*  
   *Default: 0*
 
 - kNEOMDecodingParameter1DCode93Enable  
   **(BOOL)**  
   *Enable Code 93 symbology.*  
   *Default: NO*
 
 - kNEOMDecodingParameter1DCode128Enable  
   **(BOOL)**  
   *Enable Code 128 symbology.*  
  *Default: YES*
 
 - kNEOMDecodingParameter1DCode128Codesize  
   **(Integer: 0 - 255)**  
   *Required length of Code 128 codes. 0 means any length.*  
   *Default: 0*
 
 - kNEOMDecodingParameter1DCodabarEnable  
   **(BOOL)**  
   *Enable Codabar symbology.*  
   *Default: NO*
 
 - kNEOMDecodingParameter1DCode11Enable  
   **(BOOL)**  
   *Enable Code 11 symbology.*  
   *Default: NO*
 
 - kNEOMDecodingParameter1DCode11TwoCheckchars  
   **(BOOL)**  
   *If enabled, two check digits are required for Code 11 codes (type 'C' and type 'K'). Otherwise only the type 'C' check digit is required.*  
   *Default: NO*
 
 - kNEOMDecodingParameter1DChinesePostCodeEnable  
   **(BOOL)**  
   *Enable Chinese Post Code symbology.*  
   *Default: NO*
 
 - kNEOMDecodingParameter1DDecodingStrategy  
   **(Integer: 0 - 1)**  
   *0: Decode the first barcode which is found*  
   *1: Decode the barcode which is closest to the center of the image*  
   *Default: 0*
 
 **Example**
	NSDictionary *params = @{kNEOMDecodingParameter1DITFEnable:@YES,
							 kNEOMDecodingParameter1DITFCodesize:@6,
							};
	[NEOMViewfinderViewController setDecodingParameters:params];
 
 @param parameters	An NSDictionary containing a list of parameters as pairs of parameter name and value.
 */
+ (void)setDecodingParameters:(NSDictionary *)parameters;

///---------------------------------------------------------------------------------------
/// @name Optional additional functionality
///---------------------------------------------------------------------------------------

/**
 Triggers the autofocus.
 In case the device has no autofocus capabilities, the [NEOMViewfinderViewControllerDelegate viewfinder:encounteredError:] message is called.
 */
- (void)autofocus;

/** Method to get to know if there is a camera available at the specified physical position.
 
 When passing `AVCaptureDevicePositionUnspecified`, the method checks if there is a camera available at all.
 @param cameraPosition A value of the `AVCaptureDevicePosition` enum.
 @return Returns `YES` if one of the available cameras is located at the specified physical position. Returns also `YES` when `AVCaptureDevicePositionUnspecified` was passed and there is a camera available at any position. `NO` is returned otherwise.
 */
- (BOOL)isCameraAvailableAtPosition:(AVCaptureDevicePosition)cameraPosition;

/** Method returns the position of the used video camera.
 @return Returns one of the possible `AVCaptureDevicePosition` enum values.
 */
- (AVCaptureDevicePosition)cameraPosition;

/** Method to select the physical camera position the viewfinder should use for display and decoding.
 
  When passing `AVCaptureDevicePositionUnspecified`, the default camera is set. This is typically the back camera in case the device has both, a front and a back camera. Please note that in this case the cameraPosition value will be set to the actual position of the default camera, which can be either back or front.
 @param cameraPosition A value of the `AVCaptureDevicePosition` enum.
 @return Returns `YES` if changing the camera position was successful. In case of an error, `NO` is returned and the [NEOMViewfinderViewControllerDelegate viewfinder:encounteredError:] message is called with a corresponding error code from `NEOMErrorDomain`.
*/
- (BOOL)setCameraPosition:(AVCaptureDevicePosition)cameraPosition;

/** Method to get to know if NeoMedia branding of the Viewfinder is turned off.
 @return Returns `YES` if the NeoMedia branding in the Viewfinder was successfully turned off, i.e. the NeoMedia Technologies logo is not displayed in the Viewfinder. `NO` is returned otherwise.
 */
- (BOOL)isNeomBrandingOff;

/** Method to turn the NeoMedia Technologies branding off (i.e. remove the
 NeoMedia logo from the NEOMViewfinderViewController).
 
 @param license A valid NEOMLicense instance which has the _Turn Branding off_ feature unlocked. To check if a given license has this feature unlocked, call [NEOMLicense isBrandingOffUnlocked].
 @exception NSInvalidArgumentException Thrown if the passed license is nil.
 @exception NEOMInsufficientLicenseException Thrown if the passed license is invalid or if the _Turn Branding off_ feature is not unlocked in the license.
 */
- (void)setNeomBrandingOffWithLicense:(NEOMLicense *)license;

/** An integer specifying how many frames to skip before sending the next frame
 to the decoding engines.
 
 NEOMViewfinderViewController is configured to process as many frames as possible
 by default. However, this means that the decoding methods called by the NeoReader
 SDK will lead to a very high CPU usage. This may be an issue for apps that add
 UI elements while scanning, for instance Augmented Reality Apps.
 Use this method to skip frames if CPU usage is too high for your app. The skipped
 frames won't be decoded by the engines.
 
 The value of this property can be changed at any time, even when livestreamDecoding is activated.
 
 The default value for this property is `0`.
 */
@property(assign) NSUInteger framesToSkipBeforeProcessingNext;

/** Limits decoding to a rectangle area of the visible viewfinder image.
 
 The decoding area can be restricted to an rectangle area at the center of the
 viewfinder. The size of this rectangle is given as fractions of the long edge
 and the short edge of the viewfinder. If a value of `1.0` is given for both
 edges, the whole visibile area is scanned for codes. This is the default
 setting.
 
 In case of an square viewfinder image, the longEdge value referes to the edge
 of the viewfinder, which is parallel to the long edge of the display.
 */
@property(assign) NEOMDecodingRectangleSize decodingRectangleSize;

/** The current decoding area as an CGRect describing an rectangle in the viewfinder's view. */
@property(assign, readonly) CGRect decodingRect;

/** The opacity of the grayed out area surrounding the decoding rectangle.
 
 The value of this property must in the range from `0.0` (transparent) to
 `1.0` (opaque).
 
 The default value for this property is `0.5`.
 */
@property(assign) CGFloat decodingFrameOpacity;

///---------------------------------------------------------------------------------------
/// @name Properties
///---------------------------------------------------------------------------------------

/** The delegate of the viewfinder. Needs to conform to the NEOMViewfinderViewControllerDelegate protocol. */
@property(assign) id<NEOMViewfinderViewControllerDelegate> delegate;

/** Specifies if the 1D barcode decoding engine is used for livestream or snapshot decoding. */
@property(assign, getter=is1DEngineUsed) BOOL use1DEngine;

/** Specifies if the Data Matrix decoding engine is used for livestream or snapshot decoding. */
@property(assign, getter=isDMEngineUsed) BOOL useDMEngine;

/** Specifies if the QR code decoding engine is used for livestream or snapshot decoding. */
@property(assign, getter=isQREngineUsed) BOOL useQREngine;

/** Specifies if the Aztec decoding engine is used for livestream or snapshot decoding. */
@property(assign, getter=isAztecEngineUsed) BOOL useAztecEngine;

/** Specifies if the PDF417 decoding engine is used for livestream or snapshot decoding. */
@property(assign, getter=isPDF417EngineUsed) BOOL usePDF417Engine;

/** Use to check if the camera is initialized and ready to use for livestream output and taking snapshots.
`YES` after successful execution of openCamera.
`NO` after successful execution of releaseCamera.*/
@property(assign, readonly, getter=isCameraOpened) BOOL cameraOpened;

/** Use to check if the livestream preview is running.
`YES` after successful execution of startLivestream.
`NO` after successful execution of stopLivestream.*/
@property(assign, readonly, getter=isLivestreaming) BOOL livestreaming;

/** Use to check if livestream decoding is activated. 
`YES` after successful execution of startLivestreamDecoding.
`NO` after successful execution of stopLivestreamDecoding.*/
@property(assign, readonly, getter=isLivestreamDecoding) BOOL livestreamDecoding;


///---------------------------------------------------------------------------------------
/// @name Wrapped AVFoundation methods & properties
///---------------------------------------------------------------------------------------

/** Returns a Boolean value that indicates whether the preset is supported.
 
 The Boolean value returned by this method indicates whether the preset is
 supported by the executing device and it's iOS version.
 
 Please notice that the internal decoding engines don't support resolutions that
 exceed 1024 pixel in any dimension. If you choose a preset associated with a
 higher camera resolution, decodingRectangleSize will be adjusted automatically,
 so that the size of the decoding area does not exceed 1024 pixels in any dimension.
 
 @param preset A preset you would like to set for the receiver. For possible
  values, see “Video Input Presets” in AVCaptureSession of the iOS API reference.
 @return Returns `YES` if the receiver can use preset, otherwise NO.
 */
- (BOOL)canSetCaptureSessionPreset:(NSString *)preset;


/** A constant value indicating the quality level (video resolution) of the frames sent to the decoder.
 
 This property is a wrapper of AVCaptureSession's sessionPreset property.
 For possible values of captureSessionPreset, see “Video Input Presets” in
 AVCaptureSession of the iOS API reference.
 The default value is AVCaptureSessionPreset640x480 in case it is supported by
 the used device. If it is not supported, AVCaptureSessionPresetMedium is used
 as fallback.
 
 Please notice that the internal decoding engines don't support resolutions that
 exceed 1024 pixel in any dimension. If you choose a preset associated with a
 higher camera resolution, decodingRectangleSize will be adjusted automatically,
 so that the size of the decoding area does not exceed 1024 pixels in any dimension.
 
 Calls the delegate's viewfinder:encounteredError: method in case the specified
 session preset is not supported by the device or if the resolution of the preset
 is too high for the decoding engines. Call canSetCaptureSessionPreset: 
 beforehand, to verify the desired preset is supported.
 */
@property(copy) NSString *captureSessionPreset;

/** Property to access or change the current exposure mode for the capture device.
 
 Calls the delegate's viewfinder:encounteredError: method in case the new exposure mode set is not supported by the device.
 */
@property(assign) AVCaptureExposureMode exposureMode;

/** Property to access or change the current focus mode for the capture device.
 
 Calls the delegate's viewfinder:encounteredError: method in case the new focus mode set is not supported by the device.
 */
@property(assign) AVCaptureFocusMode focusMode;

/**
 Property to ask the capture device whether it supports autofocus. (read-only)
 */
@property(nonatomic, assign, readonly) BOOL hasAutofocus;

/**
 Property to ask the capture device whether it has a torch. (read-only)
 */
@property(nonatomic, assign, readonly) BOOL hasTorch;

/** Property to access or change the current torch mode for the capture device.
 
 Calls the delegate's viewfinder:encounteredError: method in case the new torch mode set is not supported by the device.
 */
@property(assign) AVCaptureTorchMode torchMode;

/** Property to access or change the current whitebalance mode for the capture device.
 
 Calls the delegate's viewfinder:encounteredError: method in case the new whitebalance mode set is not supported by the device.
 */
@property(assign) AVCaptureWhiteBalanceMode whiteBalanceMode;

@end

/**
 Enum NEOMViewfinderErrors
 */
typedef enum {
	kNEOMViewfinderErrorUnknown								= 100,
	kNEOMViewfinderErrorUnspecified							= 101,
	kNEOMViewfinderErrorNoVideoCaptureDeviceAvailable		= 102,
	kNEOMViewfinderErrorCameraOutputFormatUnavailable		= 103,
	kNEOMViewfinderErrorCameraNotOpened						= 104,
	kNEOMViewfinderErrorCameraPositionNotAvailable			= 105,
	kNEOMViewfinderErrorLivestreamNotRunning				= 106,
	kNEOMViewfinderErrorStillImageConnectionUnavailable		= 107,
	kNEOMViewfinderErrorStillImageBufferInvalid				= 108,
	kNEOMViewfinderErrorCaptureSessionCantAddCamera			= 109,
	kNEOMViewfinderErrorCaptureSessionPresetNotSupported	= 110,
	kNEOMViewfinderErrorAutoexposureNotAvailable			= 111,
	kNEOMViewfinderErrorAutofocusNotAvailable				= 112,
	kNEOMViewfinderErrorAutowhitebalanceNotAvailable		= 113,
	kNEOMViewfinderErrorExposureModeNotSupported			= 114,
	kNEOMViewfinderErrorFocusModeNotSupported				= 115,
	kNEOMViewfinderErrorTorchModeNotSupported				= 116,
	kNEOMViewfinderErrorWhitebalanceModeNotSupported		= 117
} NEOMViewfinderErrors;

/** This protocol describes the interface that a delegate of an 
 NEOMViewfinderViewController instance must adopt to respond to viewfinder
 decoding and error events. Note that the NSError objects sent by the
 Viewfinder are from the `NEOMErrorDomain`, and the corresponding error
 codes are specified in the `NEOMViewfinderErrors` enum.
 
 Description of the `NEOMViewfinderErrors` enum, part of the `NEOMErrorDomain`:
	typedef enum {
		kNEOMViewfinderErrorUnknown								= 100,	
		kNEOMViewfinderErrorUnspecified							= 101,	
		kNEOMViewfinderErrorNoVideoCaptureDeviceAvailable		= 102,	
		kNEOMViewfinderErrorCameraOutputFormatUnavailable		= 103,	
		kNEOMViewfinderErrorCameraNotOpened						= 104,	
		kNEOMViewfinderErrorCameraPositionNotAvailable			= 105,
		kNEOMViewfinderErrorLivestreamNotRunning				= 106,
		kNEOMViewfinderErrorStillImageConnectionUnavailable		= 107,
		kNEOMViewfinderErrorStillImageBufferInvalid				= 108,
		kNEOMViewfinderErrorCaptureSessionCantAddCamera			= 109,
		kNEOMViewfinderErrorCaptureSessionPresetNotSupported	= 110,
		kNEOMViewfinderErrorAutoexposureNotAvailable			= 111,
		kNEOMViewfinderErrorAutofocusNotAvailable				= 112,
		kNEOMViewfinderErrorAutowhitebalanceNotAvailable		= 113,
		kNEOMViewfinderErrorExposureModeNotSupported			= 114,
		kNEOMViewfinderErrorFocusModeNotSupported				= 115,
		kNEOMViewfinderErrorTorchModeNotSupported				= 116,
		kNEOMViewfinderErrorWhitebalanceModeNotSupported		= 117
	} NEOMViewfinderErrors;
 
 */
@protocol NEOMViewfinderViewControllerDelegate <NSObject>
@optional
/** Sent when the Viewfinder encountered an error at runtime.
 @param viewfinder	The viewfinder view controller that sent the message.
 @param error		The error that occured. Error's are part of the `NEOMErrorDomain`. The error codes are values of the `NEOMViewfinderErrors` enum described above.
*/
- (void)viewfinder:(NEOMViewfinderViewController *)viewfinder encounteredError:(NSError *)error;

/** Sent when the Viewfinder successfully decoded a code found via livestream decoding.
 @param viewfinder	The viewfinder view controller that sent the message.
 @param result		The NEOMCode object corresponding to the scanned code. 
 Note that the code object isn't parsed automatically by the Parser module. 
 For that reason, the code's NEOMCodeParameters property is nil at this point.
*/
- (void)viewfinder:(NEOMViewfinderViewController *)viewfinder didDecodeLivestreamWithResult:(NEOMCode *)result;

/** Sent when the Viewfinder successfully decoded a code found in a snapshot.
 @param viewfinder	The viewfinder view controller that sent the message.
 @param result		The NEOMCode object corresponding to the scanned code. 
 Note that the code object isn't parsed automatically by the Parser module. 
 For that reason, the code's NEOMCodeParameters property is nil at this point.
*/
- (void)viewfinder:(NEOMViewfinderViewController *)viewfinder didDecodeSnapshotWithResult:(NEOMCode *)result;

/** Sent when the Viewfinder processed a livestream image, but could not find a code with the activated decoding engines.
 @param viewfinder	The viewfinder view controller that sent the message.
*/
- (void)viewfinderFailedToDecodeLivestream:(NEOMViewfinderViewController *)viewfinder;

/** Sent when the Viewfinder processed a snapshot image, but could not find a code with the activated decoding engines.
 @param viewfinder	The viewfinder view controller that sent the message.
*/
- (void)viewfinderFailedToDecodeSnapshot:(NEOMViewfinderViewController *)viewfinder;

/** Sent when the Viewfinder's decoding area has changed.
 
 This message is sent in the following situations:
 
 1. [NEOMViewfinderViewController decodingRectangleSize] was changed.
 2. The device was rotated.
 3. [NEOMViewfinderViewController decodingRectangleSize] was adjusted automatically, because the selected capture resolution exceeds the limit of the internal decoding engines, cf. to [NEOMViewfinderViewController captureSessionPreset].
 
 @param viewfinder		The viewfinder view controller that sent the message.
 @param decodingRect	The new decoding rectangle.
 */
- (void)viewfinder:(NEOMViewfinderViewController *)viewfinder changedDecodingRectTo:(CGRect)decodingRect;
@end

