//
//  NEOMLicense.h
//  NeoReaderSDK
//
//  Created by Martin Wermers on 21.07.11.
//  Copyright 2011 NeoMedia Europe GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 Class that provides functionality to create and use a License object that is required
 by the NeoReaderSDK modules NEOMViewfinderViewController and NEOMCodeParser.
 */
@interface NEOMLicense : NSObject <NSCoding>

///---------------------------------------------------------------------------------------
/// @name Obtaining & Updating License data
///---------------------------------------------------------------------------------------

/** Download license data from the NeoMedia license server.
 
 Use the license data returned by this message as parameter in initWithApplicationID:licenseData:customerKey: .
 @param anApplicationID Unique application identifier provided by NeoMedia.
 @param aCustomerKey 256 bit AES key of the SDK customer that will be used to ensure a secure communication to the server. Key provided by NeoMedia.
 @param requestSettings The license request settings used for the download. Specifies which features the license should unlock.
 The dictionary must contain boolean NSNumber values for the keys specified in `NEOMLicenseRequestSettings.h`. 

 *Notice:* Requested features that are not unlocked on the server side won't be unlocked in the downloaded license. On the other hand, features that are unlocked on the server side, but are not requested in the request settings dictionary, won't be unlocked in the downloaded license either.
 @param error Out parameter used if an error occurs while processing the download request. Optional parameter. May be `NULL`.
 @return NSData object containing the license data.
 Returns `nil` if a connection to the NeoMedia license server could not be created or if the download fails.
 @exception NSInvalidArgumentException Thrown if one of the passed mandatory parameters is `nil` or a passed mandatory string is empty.
 @exception NEOMLicenseServerException Thrown if the server connection was successful, but the downloaded data is corrupt.
*/
+ (NSData *)downloadLicenseForApplicationID:(NSString *)anApplicationID customerKey:(NSData *)aCustomerKey requestSettings:(NSDictionary *)requestSettings error:(NSError **)error;

/**
 Refreshes the license data of a previously created license instance, for example after downloading a new license.
 
 The license needs to fit to the application id and customer key set on initialization of the NEOMLicense instance.
 @param theLicenseData	License data received from NeoMedia's license server by calling the downloadLicenseForApplicationID:customerKey:requestSettings:error: method.
 */
- (void)refreshLicenseData:(NSData *)theLicenseData;

///---------------------------------------------------------------------------------------
/// @name Initialization & disposal
///---------------------------------------------------------------------------------------

/**
 Creates a new License object with a customer key-encrypted license key that 
 was received from the license server. 
 
 @param anApplicationID Unique application identifier that is provided by NeoMedia.
 @param theLicenseData	License data received from NeoMedia's license server by calling the downloadLicenseForApplicationID:customerKey:requestSettings:error: method.
 This data is encrypted with the customer's 256 bit AES key.
 @param aCustomerKey 256 bit AES key of the SDK customer provided by NeoMedia. Will be used in this method to decrypt the license data.
   The key has to be the appropriate one regarding the other parameters - application identifier and license data.
 @return Returns the initialized instance.
 @exception	NSInvalidArgumentException Thrown if one of the passed parameters is `nil` or a passed string is empty.
 @exception NEOMInvalidKeyException Thrown if the passed customer key is not 256 bit long.
*/
- (id)initWithApplicationID:(NSString *)anApplicationID licenseData:(NSData *)theLicenseData customerKey:(NSData *)aCustomerKey;

///---------------------------------------------------------------------------------------
/// @name Information Retrieval
///---------------------------------------------------------------------------------------

/**
 Checks if this license is valid.

 A license is considered valid if the license

   - contains the same product name as the used NeoReaderSDK.
   - contains the same major and minor version as the used NeoReader SDK (revision is ignored).
   - targets the current iOS device, i.e. if the license contains the unique identifier of the iOS device that executes SDK code.
   - is not expired in date and time.

 @return `YES` if the license is valid; `NO` otherwise.
*/
- (BOOL)isValid;


/**
 * Checks if the license is eligible for 1D barcode decoding.
 * @return `YES` if 1D engine is available; `NO` otherwise.
 */
- (BOOL)is1DUnlocked;


/**
 * Checks if the license is eligible for Data Matrix decoding.
 * @return `YES` if Data Matrix engine is available; `NO` otherwise.
 */
- (BOOL)isDataMatrixUnlocked;


/**
 * Checks if the license is eligible for QR decoding.
 * @return `YES` if QR engine is available; `NO` otherwise.
 */
- (BOOL)isQRUnlocked;


/**
 * Checks if the license is eligible for Aztec decoding.
 * @return `YES` if Aztec engine is available; `NO` otherwise.
 */
- (BOOL)isAztecUnlocked;


/**
 * Checks if the license is eligible for PDF417 decoding.
 * @return `YES` if PDF417 engine is available; `NO` otherwise.
 */
- (BOOL)isPDF417Unlocked;


/**
 * Checks if the license is eligible to access the viewfinder module.
 * @return `YES` if the viewfinder module is available; `NO` otherwise.
 */
- (BOOL)isViewfinderUnlocked;


/**
 * Checks if the license is eligible to access the parser module.
 * @return `YES` if the parser module is available; `NO` otherwise.
 */
- (BOOL)isParserUnlocked;

/**
 * Checks if the license is eligible to turn the NeoMedia Technologies branding
 * off (i.e. remove the NeoMedia logo from the NEOMViewfinderViewController).
 * @return `YES` if hiding the NeoMedia Technologies logo is available; `NO` otherwise.
 */
- (BOOL)isBrandingOffUnlocked;

/**
 Gets the device identifier the license is valid for.
 
 The device identifier has to match the unique identifier generated by the SDK at first time use.
 A missmatch in the device identifier is one of the reasons for a license being invalid (cf. method isValid: ).
 @return An NSString instance holding the device identifier stored in the license.
 */
- (NSString *)deviceIdentifier;

/**
 Gets the date and time at which the license expires.
 @return An NSDate instance referring to the point in time of the license expiration.
 */
- (NSDate *)expirationDate;

/**
 Gets the SDK product name this license is valid for.
 
 Call this method to check if the product name of the license matches the product name returned by `[NEOMNeoReaderSDK productName]`. A missmatch in product name is one of the reasons for a license being invalid (cf. method isValid: ).
 @return An NSString instance holding the product name stored in the license.
 */
- (NSString *)productName;

/**
 Gets the SDK version this license is valid for.
 
 Call this method to check if the version number of the license matches the version number returned by `[NEOMNeoReaderSDK sdkVersion]`. A version missmatch is one of the reasons for a license being invalid (cf. method isValid: ).
 @return An NSString instance holding the version number stored in the license.
 */
- (NSString *)version;

///---------------------------------------------------------------------------------------
/// @name Properties
///---------------------------------------------------------------------------------------

/** The encrypted license data received from NeoMedia's license server. */
@property (nonatomic, retain, readonly) NSData *licenseData;

@property (nonatomic, retain, readonly) NSData *customerDecryptedLicenseData;

@end
