//
//  NEOMAppstoreCodeParameters.h
//  NeoReaderSDK
//
//  Created by Martin Wermers on 21.07.11.
//  Copyright 2011 NeoMedia Europe GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NEOMCodeParameters.h"

/**
 An object of this class is returned by [NEOMCodeParser parseCode:withLicense:]
 if the parsed code contains a link to an Appstore product.

 */
@interface NEOMAppstoreCodeParameters : NEOMCodeParameters

///---------------------------------------------------------------------------------------
/// @name Initialization & disposal
///---------------------------------------------------------------------------------------

/** Initializes a newly allocated object with the strings provided in the parameters.
 
 @param aTitle 				Title string of the code.
 @param aContentString		Content string of the code.
 @param aProductIDString	Appstore ID of the linked product found in the code's content part. 
 @return 					Returns the initialized instance.
 */
- (id)initWithTitle:(NSString *)aTitle content:(NSString *)aContentString productID:(NSString *)aProductIDString;

///---------------------------------------------------------------------------------------
/// @name Properties
///---------------------------------------------------------------------------------------

/** Appstore ID of the linked product found in the code's content part.
 
 *Example:* If the code includes the link itms://itunes.apple.com/app/id385145768, the productID is 385145768. 
 */
@property(nonatomic, copy, readonly) NSString *productID;

@end
