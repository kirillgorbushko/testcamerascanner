//
//  NEOMSMSCodeParameters.h
//  NeoReaderSDK
//
//  Created by Martin Wermers on 21.07.11.
//  Copyright 2011 NeoMedia Europe GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NEOMCodeParameters.h"

/**
 An object of this class is returned by [NEOMCodeParser parseCode:withLicense:]
 if the parsed code is a SMS-code. 
 */
@interface NEOMSMSCodeParameters : NEOMCodeParameters <NEOMCodeParametersProtocol>

///---------------------------------------------------------------------------------------
/// @name Initialization & disposal
///---------------------------------------------------------------------------------------

/** Initializes a newly allocated object with the strings provided in the parameters.
 
 @param aTitle 				Title string of the code.
 @param aContentString		Content string of the code.
 @param aRecipient			Phone number of the SMS recipient found in the code's content part.
 @param aBody				SMS message body found in the code's content part.
 @return 					Returns the initialized instance.
 */
- (id)initWithTitle:(NSString *)aTitle content:(NSString *)aContentString recipient:(NSString *)aRecipient body:(NSString *)aBody;

///---------------------------------------------------------------------------------------
/// @name Properties
///---------------------------------------------------------------------------------------

/** Phone number of the SMS recipient found in the code's content part. */
@property(nonatomic, copy, readonly) NSString *recipient;

/** SMS message body found in the code's content part. */
@property(nonatomic, copy, readonly) NSString *body;

@end
