//
//  NEOMPersonName.h
//  NeoReaderSDK
//
//  Created by Martin Wermers on 19.03.12.
//  Copyright (c) 2012 NeoMedia Europe GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AddressBook/AddressBook.h>

/**
 Objects of this class are referenced from within NEOMCardCodeParameters objects.
 */

@interface NEOMPersonName : NSObject

///---------------------------------------------------------------------------------------
/// @name Initialization & disposal
///---------------------------------------------------------------------------------------

/** Initializes a newly allocated object with the strings provided in the parameters.
 
 @param aFamilyName		Family name part of the full name.
 @param aGivenName		Given name part of the full name.
 @param aMiddleName		Middle name part of the full name.
 @param aNamePrefix		Name prefix of the full name (e.g. "Dr. rer. nat.").
 @param aNameSuffix		Name suffix of the full name (e.g. "Jr.").
 @return 				Returns the initialized instance.
 */
- (id)initWithFamilyName:(NSString *)aFamilyName givenName:(NSString *)aGivenName middleName:(NSString *)aMiddleName namePrefix:(NSString *)aNamePrefix nameSuffix:(NSString *)aNameSuffix;

///---------------------------------------------------------------------------------------
/// @name Comparison
///---------------------------------------------------------------------------------------
/** Returns a Boolean value that indicates whether a given NEOMPersonName instance is equal to the
 receiver using a comparison of all individual name strings.
 @param aPersonName	The instance with which to compare the receiver.
 @return 		`YES` if _aPersonName_ is equivalent to the receiver (if they have
 the same five name strings, otherwise `NO`.
 */
- (BOOL)isEqualToPersonName:(NEOMPersonName *)aPersonName;

///---------------------------------------------------------------------------------------
/// @name Helper Methods
///---------------------------------------------------------------------------------------

/** Set's all five name property values in the passed ABRecord instance.
 @param aRecord The address book record which will receive the receivers name strings.
 @return A boolean indicating if the setting of all name strings was successful.
 */
- (BOOL)setNameFieldsInABRecord:(ABRecordRef)aRecord;

/**
 Creates a formatted name string from all existing name properties, with the following sequence:
 namePrefix givenName middleName familyName nameSuffix.
 @return The formatted name NSString instance.
 */
- (NSString *)formattedName;

///---------------------------------------------------------------------------------------
/// @name Properties
///---------------------------------------------------------------------------------------

/** Family name part found in a MeCard or vCard. */
@property(nonatomic, copy, readonly) NSString *familyName;

/** Given name part found in a MeCard or vCard. */
@property(nonatomic, copy, readonly) NSString * givenName;

/** Middle name part found in a MeCard or vCard. */
@property(nonatomic, copy, readonly) NSString *middleName;

/** Name prefix found in a MeCard or vCard (e.g. "Dr. rer. nat."). */
@property(nonatomic, copy, readonly) NSString *namePrefix;

/** Name suffix found in a MeCard or vCard (e.g. "Jr."). */
@property(nonatomic, copy, readonly) NSString *nameSuffix;

@end
