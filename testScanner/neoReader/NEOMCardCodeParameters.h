//
//  NEOMCardCodeParameters.h
//  NeoReaderSDK
//
//  Created by Martin Wermers on 20.03.12.
//  Copyright (c) 2012 NeoMedia Europe GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AddressBook/AddressBook.h>

#import "NEOMCodeParameters.h"

#pragma mark Constants
extern NSString * const kNEOMCardFieldTypeUndefined;
extern NSString * const kNEOMCardFieldTypeHome;
extern NSString * const kNEOMCardFieldTypeWork;

typedef enum NEOMCardType_enum {
	kNEOMCardTypeMeCard,
	kNEOMCardTypeVCard
} NEOMCardType;

@class NEOMPersonName;

/**
 An object of this class is returned by [NEOMCodeParser parseCode:withLicense:]
 if the parsed code is a MeCard- or vCard-code.
 
 Description of the NEOMCardType enum:
     typedef enum NEOMCardType_enum {
         kNEOMCardTypeMeCard,
         kNEOMCardTypeVCard
     } NEOMCardType;
 */
@interface NEOMCardCodeParameters : NEOMCodeParameters <NEOMCodeParametersProtocol>

///---------------------------------------------------------------------------------------
/// @name Initialization & disposal
///---------------------------------------------------------------------------------------

/** Initializes a newly allocated object with the given type.
 
 @param aTitle 			Title string of the code.
 @param aContentString	Content string of the code.
 @param aType			Type of the Card from which the object is generated.
 Can be either kNEOMCardTypeMeCard or kNEOMCardTypeVCard.
 @return				Returns the initialized instance.
 */
- (id)initWithTitle:(NSString *)aTitle content:(NSString *)aContentString type:(NEOMCardType)aType;

///---------------------------------------------------------------------------------------
/// @name Helper Methods
///---------------------------------------------------------------------------------------

/** Returns a human readable string of the receivers type (MeCard or vCard).
 @return A human readable string of the card type.
 */
- (NSString *)typeString;

///---------------------------------------------------------------------------------------
/// @name Properties
///---------------------------------------------------------------------------------------

/** The type the Card from which the object is generated. */
@property(nonatomic, assign, readonly) NEOMCardType type;

/** The Address Book record representation of the MeCard or vCard. */
@property(nonatomic, assign, readonly) ABRecordRef abRecordRepresentation;

/** The name found in the MeCard or vCard. */
@property(nonatomic, retain) NEOMPersonName *name;

/** The formatted name found in the MeCard or vCard. */
@property(nonatomic, copy) NSString *formattedName;

/** A list of nicknames found in the MeCard or vCard. */
@property(nonatomic, retain) NSArray *nicknames;

/** A list of organizations found in the MeCard or vCard. */
@property(nonatomic, retain) NSArray *organizations;

/** A list of job titles found in the MeCard or vCard. */
@property(nonatomic, retain) NSArray *jobTitles;

/**
 A dictionary containing the post addresses found in the MeCard or vCard.
 
 The keys in the dictonary indicate the type of the post addresses:
 kNEOMCardFieldTypeUndefined, kNEOMCardFieldTypeHome or kNEOMCardFieldTypeWork.
 
 The value for a key is a NSArray object containing a list of NEOMPostAddress
 objects with post addresses of the corresponding type.
 */
@property(nonatomic, retain) NSDictionary *postAddresses;

/**
 A dictionary containing the email addresses found in the MeCard or vCard.
 
 The keys in the dictonary indicate the type of the email addresses:
 kNEOMCardFieldTypeUndefined, kNEOMCardFieldTypeHome or kNEOMCardFieldTypeWork.
 
 The value for a key is a NSArray object containing a list of NSString
 objects with email addresses of the corresponding type.
 */
@property(nonatomic, retain) NSDictionary *emailAddresses;

/**
 A dictionary containing the phone numbers found in the MeCard or vCard.
 
 The keys in the dictonary indicate the type of the phone numbers:
 kNEOMCardFieldTypeUndefined, kNEOMCardFieldTypeHome or kNEOMCardFieldTypeWork.
 
 The value for a key is a NSArray object containing a list of NSString
 objects with phone numbers of the corresponding type.
 */
@property(nonatomic, retain) NSDictionary *phoneNumbers;

/**
 A dictionary containing the mobile phone numbers found in the MeCard or vCard.
 
 The keys in the dictonary indicate the type of the mobile phone numbers:
 kNEOMCardFieldTypeUndefined, kNEOMCardFieldTypeHome or kNEOMCardFieldTypeWork.
 
 The value for a key is a NSArray object containing a list of NSString
 objects with mobile phone numbers of the corresponding type.
 */
@property(nonatomic, retain) NSDictionary *mobileNumbers;

/**
 A dictionary containing the fax numbers found in the MeCard or vCard.
 
 The keys in the dictonary indicate the type of the fax numbers:
 kNEOMCardFieldTypeUndefined, kNEOMCardFieldTypeHome or kNEOMCardFieldTypeWork.
 
 The value for a key is a NSArray object containing a list of NSString
 objects with fax numbers of the corresponding type.
 */
@property(nonatomic, retain) NSDictionary *faxNumbers;

/**
 A dictionary containing the URLs found in the MeCard or vCard.
 
 The keys in the dictonary indicate the type of the URLs:
 kNEOMCardFieldTypeUndefined, kNEOMCardFieldTypeHome or kNEOMCardFieldTypeWork.
 
 The value for a key is a NSArray object containing a list of NSString
 objects with URLs of the corresponding type.
 */
@property(nonatomic, retain) NSDictionary *URLs;

/** The birthday found in the MeCard or vCard. */
@property(nonatomic, retain) NSDate *birthday;

/** The note found in the MeCard or vCard. */
@property(nonatomic, copy) NSString *note;

@end
