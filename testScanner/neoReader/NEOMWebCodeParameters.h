//
//  NEOMWebCodeParameters.h
//  NeoReaderSDK
//
//  Created by Martin Wermers on 21.07.11.
//  Copyright 2011 NeoMedia Europe GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NEOMCodeParameters.h"

/**
 An object of this class is returned by [NEOMCodeParser parseCode:withLicense:]
 if the parsed code is a web url-code. 
 */

@interface NEOMWebCodeParameters : NEOMCodeParameters <NEOMCodeParametersProtocol>

///---------------------------------------------------------------------------------------
/// @name Initialization & disposal
///---------------------------------------------------------------------------------------

/** Initializes a newly allocated object with the strings provided in the parameters.
 
 @param aTitle 				Title string of the code.
 @param aContentString		Content string of the code.
 @param aURLString			URL string found in the code's content part.
 @return 					Returns the initialized instance.
 */
- (id)initWithTitle:(NSString *)aTitle content:(NSString *)aContentString URLString:(NSString *)aURLString;

///---------------------------------------------------------------------------------------
/// @name Properties
///---------------------------------------------------------------------------------------

/** URL string found in the code's content part. */
@property(nonatomic, copy, readonly) NSString *URLString;

@end
