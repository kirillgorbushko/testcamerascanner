//
//  NeoReaderViewController.swift
//  testScanner
//
//  Created by Ivan.Pavliuk on 7/26/18.
//  Copyright © 2018 - present. All rights reserved.
//

import UIKit

class NeoReaderViewController: UIViewController {
    
    var customerKey: [UInt8] = [0xdc, 0x75, 0xe9, 0x10,
                       0x8a, 0xe2, 0xa2, 0x6c,
                       0x33, 0xdb, 0x85, 0xe2,
                       0xbf, 0x81, 0x72, 0xb1,
                       0xc9, 0x59, 0x07, 0x8a,
                       0xf2, 0x34, 0x88, 0x22,
                       0x9b, 0xad, 0xb4, 0xd7,
                       0x9b, 0x98, 0x15, 0xcf]
    let appId = "14584"
    let NEOMUserDefaultsLicenseDataKey = "NEOMUserDefaultsLicenseDataKey"

    let viewfinderViewController = NEOMViewfinderViewController()
    var sdkLicense: NEOMLicense?
    let lastScannedCode = NEOMCode()
    var autofocusTimer = Timer()

    override func viewDidLoad() {
        super.viewDidLoad()

        view.addSubview(viewfinderViewController.view)

        let customerKeyData = Data(bytes: customerKey)
        sdkLicense = receiveLicenseForApplicationID(applicationID: appId, customerKeyData: customerKeyData)
        guard let sdkLicense = sdkLicense else {
            fatalError("NO LICENSE")
        }

        viewfinderViewController.delegate = self

        viewfinderViewController.is1DEngineUsed = sdkLicense.is1DUnlocked()
        viewfinderViewController.isDMEngineUsed = sdkLicense.isDataMatrixUnlocked()
        viewfinderViewController.isQREngineUsed = sdkLicense.isQRUnlocked()
        viewfinderViewController.isAztecEngineUsed = sdkLicense.isAztecUnlocked()
        viewfinderViewController.isPDF417EngineUsed = sdkLicense.isPDF417Unlocked()
        
        if sdkLicense.isBrandingOffUnlocked() {
            viewfinderViewController.setNeomBrandingOffWith(sdkLicense)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if !viewfinderViewController.isLivestreaming {
            viewfinderViewController.startLivestream()
        }

        viewfinderViewController.startLivestreamDecoding(with: sdkLicense)
        if viewfinderViewController.hasAutofocus {
            startAutofocusTimer()
        }
    }
    
    deinit {
        autofocusTimer.invalidate()
    }

    private func receiveLicenseForApplicationID(applicationID: String, customerKeyData: Data) -> NEOMLicense? {
        var theLicense: NEOMLicense? = nil

        var theLicenseData = UserDefaults.standard.data(forKey: NEOMUserDefaultsLicenseDataKey)
        if let licenseData = theLicenseData {
            theLicense = NEOMLicense.init(applicationID: applicationID, license: licenseData, customerKey: customerKeyData)
            if theLicense?.isValid() == true {
                return theLicense
            } else {
                theLicense = nil
            }
        }
        
        let requestSettings = [kNEOM1DEngineLicenseRequestKey: true,
                               kNEOMDMEngineLicenseRequestKey: true,
                               kNEOMQREngineLicenseRequestKey: true,
                               kNEOMAztecEngineLicenseRequestKey: true,
                               kNEOMPDF417EngineLicenseRequestKey: true,
                               kNEOMViewfinderModuleLicenseRequestKey: true,
                               kNEOMParserModuleLicenseRequestKey: true,
                               kNEOMBrandingOffLicenseRequestKey: true
        ]

        do {
            try theLicenseData = NEOMLicense.downloadLicense(forApplicationID: applicationID, customerKey: customerKeyData, requestSettings: requestSettings)
        } catch {
            fatalError("Can not load a license")
        }

        theLicense = NEOMLicense.init(applicationID: applicationID, license: theLicenseData, customerKey: customerKeyData)
        if theLicense?.isValid() == true {
            UserDefaults.standard.set(theLicenseData, forKey: NEOMUserDefaultsLicenseDataKey)
            UserDefaults.standard.synchronize()

            return theLicense
        }
        fatalError("Can not create a license")
    }

    private func showAllert(with title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
            self.dismiss(animated: true, completion: nil)
            self.viewfinderViewController.startLivestreamDecoding(with: self.sdkLicense)
        }))
        self.present(alert, animated: true, completion: nil)
    }

    private func startAutofocusTimer() {
        autofocusTimer = Timer.scheduledTimer(timeInterval: 2.0, target: viewfinderViewController, selector: #selector(viewfinderViewController.autofocus), userInfo: nil, repeats: true)
    }
}

extension NeoReaderViewController: NEOMViewfinderViewControllerDelegate {

    func viewfinder(_ viewfinder: NEOMViewfinderViewController!, encounteredError error: Error!) {
        showAllert(with: "ERROR", message: error.localizedDescription)
        viewfinderViewController.stopLivestreamDecoding()
    }

    func viewfinder(_ viewfinder: NEOMViewfinderViewController!, didDecodeLivestreamWithResult result: NEOMCode!) {
        showAllert(with: "SUCCESS", message: result.codeString)
        viewfinderViewController.stopLivestreamDecoding()
    }
}
