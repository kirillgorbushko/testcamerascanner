//
//  NEOMCodeParametersProtocol.h
//  NeoReaderSDK
//
//  Created by Martin Wermers on 04.01.12.
//  Copyright (c) 2012 NeoMedia Europe GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

/** Description of the NEOMCodeParametersFormat enum:
     typedef enum {
          kUNKNOWN_FORMAT = -1;
         kAPPSTORE_FORMAT =  0,
         kCALENDAR_FORMAT =  1,
             kCARD_FORMAT =  2,
            kEMAIL_FORMAT =  3,
           kMAILTO_FORMAT =  4,
           kMARKET_FORMAT =  5,
           kMATMSG_FORMAT =  6,
            kMEBKM_FORMAT =  7,
            kPHONE_FORMAT =  8,
              kSMS_FORMAT =  9,
            kSMSTO_FORMAT = 10,
             kSMTP_FORMAT = 11,
              kWEB_FORMAT = 12,
             kWIFI_FORMAT = 13
     } NEOMCodeParametersFormat;
 */
typedef enum NEOMCodeParametersFormat_enum {
	 kUNKNOWN_FORMAT = -1,
	kAPPSTORE_FORMAT =  0,
	kCALENDAR_FORMAT =  1,
	    kCARD_FORMAT =  2,
	   kEMAIL_FORMAT =  3,
	  kMAILTO_FORMAT =  4,
	  kMARKET_FORMAT =  5,
	  kMATMSG_FORMAT =  6,
	   kMEBKM_FORMAT =  7,
	   kPHONE_FORMAT =  8,
	     kSMS_FORMAT =  9,
	   kSMSTO_FORMAT = 10,
	    kSMTP_FORMAT = 11,
	     kWEB_FORMAT = 12,
	    kWIFI_FORMAT = 13
} NEOMCodeParametersFormat;
#define NEOMCodeParametersFormatMin -1
#define NEOMCodeParametersFormatMax 13

#define NEOMCodeParametersCharacterEncoding @"UTF-8";

@protocol NEOMCodeParametersProtocol
@required
- (NEOMCodeParametersFormat)format;
@end
