//
//  NEOMPostAddress.h
//  NeoReaderSDK
//
//  Created by Martin Wermers on 19.03.12.
//  Copyright (c) 2012 NeoMedia Europe GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AddressBook/AddressBook.h>

/**
 Objects of this class are referenced from within NEOMCardCodeParameters objects.
 */

@interface NEOMPostAddress : NSObject

///---------------------------------------------------------------------------------------
/// @name Initialization & disposal
///---------------------------------------------------------------------------------------

/** Initializes a newly allocated object with the strings provided in the parameters.
 
 @param aPoBox				Post Office box part of the full address.
 @param anExtendedAddress	Extended Address part of the full address.
 @param aStreet				Street part of the full address.
 @param aCity				City part of the full address.
 @param aRegion				Region part of the full address.
 @param aPostcode			Postcode part of the full address.
 @param aCountryName		Country part of the full address.
 @return 					Returns the initialized instance.
 */
- (id)initWithPoBox:(NSString *)aPoBox extendedAddress:(NSString *)anExtendedAddress street:(NSString *)aStreet city:(NSString *)aCity region:(NSString *)aRegion postcode:(NSString *)aPostcode countryName:(NSString *)aCountryName;

///---------------------------------------------------------------------------------------
/// @name Comparison
///---------------------------------------------------------------------------------------
/** Returns a Boolean value that indicates whether a given NEOMPostAddress instance is equal to the
 receiver using a comparison of all individual address field strings.
 @param aPostAddress	The instance with which to compare the receiver.
 @return 		`YES` if _aPostAddress_ is equivalent to the receiver (if they have
 the same seven address field strings, otherwise `NO`.
 */
- (BOOL)isEqualToPostAddress:(NEOMPostAddress *)aPostAddress;

///---------------------------------------------------------------------------------------
/// @name Helper Methods
///---------------------------------------------------------------------------------------

/** Creates a dictionary containing all existing address properties, stored under
 the corresponding address properties (e.g. city under kABPersonAddressCityKey).
 @return The dictionary. Can be passed to a kABMultiDictionaryPropertyType via
         ABMultiValueAddValueAndLabel(...).
 */
- (NSDictionary *)abDictionaryRepresentation;

///---------------------------------------------------------------------------------------
/// @name Properties
///---------------------------------------------------------------------------------------

/** Post Office box part found in a MeCard or vCard. */
@property(nonatomic, copy, readonly) NSString *poBox;

/** Extended Address part found in a MeCard or vCard. */
@property(nonatomic, copy, readonly) NSString *extendedAddress;

/** Street part found in a MeCard or vCard. */
@property(nonatomic, copy, readonly) NSString *street;

/** City part found in a MeCard or vCard. */
@property(nonatomic, copy, readonly) NSString *city;

/** Region part found in a MeCard or vCard. */
@property(nonatomic, copy, readonly) NSString *region;

/** Postcode part found in a MeCard or vCard. */
@property(nonatomic, copy, readonly) NSString *postcode;

/** Country part found in a MeCard or vCard. */
@property(nonatomic, copy, readonly) NSString *countryName;

@end
