//
//  CodeReader.swift
//  testScanner
//
//  Created by Kirill Gorbushko on 04.07.18.
//  Copyright © 2018 - present. All rights reserved.
//

import Foundation
import AVFoundation
import UIKit

//native

protocol CodeReaderDelegate: class {
    func codeReaderDidFail(_ error: Error)
    func codeReaderDidRecognizeData(_ recognizedValue: String)
}

protocol CodeReaderMetadataOutputDelegate: class {
    func codeReaderDidFail(_ error: Error)
    func codeReaderDidObtainMetadataOutputData(_ output: CMSampleBuffer)
}

final class CodeReader: NSObject {

    fileprivate enum Constants {

        enum Error {

            static let domain = "codeReader"
            static let codeReaderErrorCode = 1111
        }
    }

    weak var delegate: CodeReaderDelegate?
    weak var metadataOutputDelegate: CodeReaderMetadataOutputDelegate?

    // MARK: - Properties

    fileprivate var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    fileprivate var captureSession: AVCaptureSession?
    fileprivate var qrCodeDetectingIndicatorView: UIView?
    fileprivate weak var previewLayerHolderView: UIView?

    private lazy var sessionQueue = DispatchQueue(label: "session.videoPutput")

    fileprivate (set) var isReading: Bool = false
    fileprivate var shouldStopIfRecognize: Bool = false

    // MARK: - LifeCycle
    init(_ previewView: UIView) {
        super.init()

        previewLayerHolderView = previewView
        isReading = false

        prepareSession()
        prepareScanningIndicatorView()
    }

    func setupForMetadataOutput() {
        setUpCaptureSessionOutput()
    }
}

extension CodeReader : AVCaptureMetadataOutputObjectsDelegate {

    // MARK: - AVCaptureMetadataOutputObjectsDelegate

    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        if !metadataObjects.isEmpty,
            let metaObj = metadataObjects.first as? AVMetadataMachineReadableCodeObject {
            for type in metadataTypes() {
                if metaObj.type.rawValue == type.rawValue {

                    if let barCodeObject = videoPreviewLayer?.transformedMetadataObject(for: metaObj as AVMetadataMachineReadableCodeObject) as? AVMetadataMachineReadableCodeObject {

                        DispatchQueue.main.async {
                            self.qrCodeDetectingIndicatorView?.frame = barCodeObject.bounds
                        }

                        let code = metaObj.stringValue
                        if let _ = delegate,
                            let code = code {
                            delegate?.codeReaderDidRecognizeData(code)
                        }
                        if shouldStopIfRecognize == true {
                            stopReading()
                            isReading = false
                        }
                    }
                }
            }
        } else {
            DispatchQueue.main.async {
                self.qrCodeDetectingIndicatorView?.frame = CGRect.zero
            }
        }
    }
}

extension CodeReader {

    // MARK: - Public

    func startStopReading(){
        if isReading == true {
            stopReading()
        } else {
            startReading()
        }

        isReading = !isReading
    }

    func relayoutPreview() {
        videoPreviewLayer?.frame = previewLayerHolderView?.bounds ?? CGRect.zero
    }

    class func askPermission(_ completion: @escaping ((_ granted: Bool) -> ())) {
        AVCaptureSession.checkPermissionForCamera(completion: completion)
    }

    // MARK: - Private

    fileprivate func startReading() {
        captureSession?.startRunning()
    }

    fileprivate func stopReading() {
        captureSession?.stopRunning()
    }

    fileprivate func prepareScanningIndicatorView() {
        qrCodeDetectingIndicatorView = UIView(frame: CGRect.zero)
        qrCodeDetectingIndicatorView?.layer.borderColor = UIColor.green.cgColor
        qrCodeDetectingIndicatorView?.layer.borderWidth = 1
        if let qrCodeDetectingIndicatorView = qrCodeDetectingIndicatorView {
            previewLayerHolderView?.addSubview(qrCodeDetectingIndicatorView)
        }
    }

    fileprivate func metadataTypes() -> [AVMetadataObject.ObjectType] {
        return [
            AVMetadataObject.ObjectType.qr,
            AVMetadataObject.ObjectType.dataMatrix,
            AVMetadataObject.ObjectType.ean13,
            AVMetadataObject.ObjectType.ean8,
            AVMetadataObject.ObjectType.code128,
            AVMetadataObject.ObjectType.upce,

            AVMetadataObject.ObjectType.aztec,
            AVMetadataObject.ObjectType.code39,
            AVMetadataObject.ObjectType.code39Mod43,
            AVMetadataObject.ObjectType.itf14,
            AVMetadataObject.ObjectType.pdf417,
            AVMetadataObject.ObjectType.interleaved2of5
        ]
    }
}

extension CodeReader {

    // MARK: - Preparation

    fileprivate func prepareSession() {

        let devices = AVCaptureDevice.DiscoverySession(deviceTypes: [AVCaptureDevice.DeviceType.builtInWideAngleCamera],
                                                       mediaType: AVMediaType.video,
                                                       position: AVCaptureDevice.Position.back).devices
        if let captureDevice = devices.first {
            do {
                let input = try AVCaptureDeviceInput(device: captureDevice) as AVCaptureDeviceInput

                if captureDevice.isFocusModeSupported(.continuousAutoFocus) == true {
                    try? captureDevice.lockForConfiguration()
                    captureDevice.focusMode = .continuousAutoFocus
                    captureDevice.unlockForConfiguration()
                }

                captureSession = AVCaptureSession()

                captureSession?.beginConfiguration()
                if captureSession?.canAddInput(input) == true {
                    captureSession?.addInput(input)

                    let metadataOutput = AVCaptureMetadataOutput()
                    if captureSession?.canAddOutput(metadataOutput) == true {
                        captureSession?.addOutput(metadataOutput)
                        captureSession?.commitConfiguration()

                        let queue = DispatchQueue(label: "com.barcodeReader.queue", attributes: [])
                        metadataOutput.setMetadataObjectsDelegate(self, queue:queue)
                        metadataOutput.metadataObjectTypes = metadataTypes()

                        if let captureSession = captureSession {
                            videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
                            videoPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
                            assert(previewLayerHolderView != nil, "invalid pointer for preview view")
                            videoPreviewLayer?.frame = previewLayerHolderView?.bounds ?? CGRect.zero
                            if let videoPreviewLayer = videoPreviewLayer {
                                previewLayerHolderView?.layer.addSublayer(videoPreviewLayer)
                            } else {
                                delegate?.codeReaderDidFail(NSError(domain: Constants.Error.domain, code: Constants.Error.codeReaderErrorCode, userInfo: [
                                    NSLocalizedDescriptionKey : "Preview layer nil"
                                    ]))
                            }
                        } else {
                            delegate?.codeReaderDidFail(NSError(domain: Constants.Error.domain, code: Constants.Error.codeReaderErrorCode, userInfo: [
                                NSLocalizedDescriptionKey : "Capture session nil during preparation process"
                                ]))
                        }
                    } else {
                        delegate?.codeReaderDidFail(NSError(domain: Constants.Error.domain, code: Constants.Error.codeReaderErrorCode, userInfo: [
                            NSLocalizedDescriptionKey : "Can't add output to capture session"
                            ]))
                    }
                } else {
                    delegate?.codeReaderDidFail(NSError(domain: Constants.Error.domain, code: Constants.Error.codeReaderErrorCode, userInfo: [
                        NSLocalizedDescriptionKey : "Can't add input to capture session"
                        ]))
                }
            } catch let error {
                delegate?.codeReaderDidFail(error)
            }
        }
    }

    fileprivate func setUpCaptureSessionOutput() {
        sessionQueue.async {
            self.captureSession?.beginConfiguration()
            self.captureSession?.sessionPreset = AVCaptureSession.Preset.photo

            let output = AVCaptureVideoDataOutput()
            output.videoSettings =
                [(kCVPixelBufferPixelFormatTypeKey as String): kCVPixelFormatType_32BGRA]
            let outputQueue = DispatchQueue(label: "outputQueue")
            output.setSampleBufferDelegate(self, queue: outputQueue)
            if self.captureSession?.canAddOutput(output) == true {
                self.captureSession?.addOutput(output)
            }
            self.captureSession?.commitConfiguration()
        }
    }
}

extension CodeReader : AVCaptureVideoDataOutputSampleBufferDelegate {

    // MARK: - AVCaptureVideoDataOutputSampleBufferDelegate
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        metadataOutputDelegate?.codeReaderDidObtainMetadataOutputData(sampleBuffer)
    }
}
